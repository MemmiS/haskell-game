{-# LANGUAGE CPP #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
{-# OPTIONS_GHC -fno-warn-implicit-prelude #-}
module Paths_project_game (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/furax/.cabal/bin"
libdir     = "/home/furax/.cabal/lib/x86_64-linux-ghc-8.0.2/project-game-0.1.0.0-3qmozMfouGfKJ2YdJtsCDi"
dynlibdir  = "/home/furax/.cabal/lib/x86_64-linux-ghc-8.0.2"
datadir    = "/home/furax/.cabal/share/x86_64-linux-ghc-8.0.2/project-game-0.1.0.0"
libexecdir = "/home/furax/.cabal/libexec"
sysconfdir = "/home/furax/.cabal/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "project_game_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "project_game_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "project_game_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "project_game_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "project_game_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "project_game_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
