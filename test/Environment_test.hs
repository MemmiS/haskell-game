module Environment_test where
import Entity
import Environment
import Equipment 
import Test.QuickCheck
import Test.Hspec
import Map_game
import qualified Data.Map as Map
import qualified Data.List as LS

instance Arbitrary Coord where
    arbitrary = gen_coord

gen_entity :: Gen Entity 
gen_entity = frequency [(100,gen_cow),(100,gen_potion),(10,gen_armor),(50,gen_gobelin),(40,gen_troll),(100,gen_player),(10,gen_key),(1,gen_chest)]

gen_pickable :: Gen Entity 
gen_pickable = frequency [(1,gen_armor),(1,gen_potion),(1,gen_key)]

gen_cow :: Gen Entity 
gen_cow  = do 
    id          <- choose (1,100000)
    pv          <- choose (1,100)
    speed       <- choose (1,10)
    stealth     <- choose (1,5) 
    armor       <- choose (1,4) 
    return $ Cow id pv speed armor stealth 0

gen_troll :: Gen Entity 
gen_troll = do 
    id          <- choose (1,100000)
    pv          <- choose (100, 10000)
    speed       <- choose (1, 8)
    stealth     <- choose (1,5)
    armor       <- choose (100, 1000)
    attack      <- choose (100,300) 
    return $ Troll id pv attack speed armor stealth 0


gen_gobelin :: Gen Entity 
gen_gobelin = do 
    id          <- choose (1,100000)
    pv          <- choose (80, 100)
    speed       <- choose (20, 50)
    stealth     <- choose (80,100)
    armor       <- choose (30, 90)
    attack      <- choose (20,50) 
    return $ Gobelin id pv attack speed armor stealth 0

gen_key :: Gen Entity 
gen_key  = do 
    id          <-  choose (1,100000)
    return $ Key id

gen_potion :: Gen Entity 
gen_potion = do 
    id          <-  choose (1,100000)
    buff        <-  elements  [Health, Water, Poison, Strength, Invisibility,Speed]
    return $ Potion id buff 


gen_helmet :: Gen Equipment 
gen_helmet = do 
    id          <- choose (1,100000)
    armor       <- choose (1,1000)
    attackPts   <- choose (100,1000)
    stealthPts  <- choose (100,1000)
    vitaPts     <- choose (1,1000)
    return $ Helmet id armor attackPts stealthPts vitaPts

gen_boots :: Gen Equipment 
gen_boots  = do 
    id          <- choose (1,100000)
    armor       <- choose (1,1000)
    speed       <- choose (100,1000)
    stealthPts  <- choose (100,1000)
    vitaPts     <- choose (1,1000)
    return $ Boots id armor speed stealthPts vitaPts


gen_gloves :: Gen Equipment 
gen_gloves = do 
    id          <-  choose (1,100000)
    armor       <-  choose (1,1000)
    attack      <-  choose (1,1000)
    vita        <-  choose (1,1000)
    return $ Gloves id armor attack vita

gen_sword :: Gen Equipment 
gen_sword = do 
    id          <-  choose (1,100000)
    attack      <-  choose (100,1000)
    return $ Sword id attack

gen_legs :: Gen Equipment 
gen_legs = do 
    id          <-  choose (1,100000)
    armor       <-  choose (1,1000) 
    stealth     <-  choose (1,1000)
    vita        <-  choose (1,1000)
    return $ Legs id armor stealth vita

gen_chest_arm :: Gen Equipment 
gen_chest_arm = do
    id      <-  choose (1,100000)
    armor   <-  choose (1,1000)
    speed   <-  choose (1,1000)
    stealth <-  choose (1,1000)
    vita    <-  choose (1,1000)
    return $ Chest_armor id armor speed stealth vita

gen_armor :: Gen Entity 
gen_armor = do
    armor <- oneof [gen_helmet, gen_boots, gen_gloves, gen_sword, gen_legs]
    return $ SetArmor armor

gen_equipment :: Gen Equipment
gen_equipment = do 
    armor <- oneof [gen_helmet, gen_boots, gen_gloves, gen_sword, gen_legs]
    return armor

gen_trap :: Gen Entity 
gen_trap =  do 
    id      <-  choose (1,100000)
    attack  <-  choose (10,100)
    return $ Trap id attack

gen_chest :: Gen Entity 
gen_chest = do 
    id      <- choose (1,100000)
    key     <- gen_key 
    nb      <- choose (1,4) -- number of armor pieces
    loot    <- vectorOf nb gen_armor
    return $ Chest id ([key] ++ loot)

gen_stuffEquipped :: Gen StuffEquipped
gen_stuffEquipped = do 
    head        <- gen_helmet
    feet        <- gen_boots
    hands       <- gen_gloves
    weapon      <- gen_sword
    legs        <- gen_legs
    chest_ar    <- gen_chest_arm
    return $ Stuff (Arm head) (Arm chest_ar) (Arm feet) (Arm hands) (Arm weapon) (Arm legs)

gen_player :: Gen Entity 
gen_player = do
    id      <- choose (1,100000)
    pvmax   <- choose (1000,10000)
    attack  <- choose (100,1000)
    armor   <- choose (100,1000)
    speed   <- choose (10,100)
    stealth <- choose (10,100)
    stuff   <- gen_stuffEquipped
    thirst_max <- choose (100,1000)
    return $ Player id pvmax pvmax attack armor speed stealth mempty stuff thirst_max thirst_max 0


gen_coord :: Gen Coord 
gen_coord = do 
    x   <-  choose (1,30)
    y   <-  choose (1,30)
    return $ C x y

gen_ls :: Gen [(Coord, [Entity])]
gen_ls = do 
    nb  <-  choose (10,100)
    vectorOf nb ls
    where   ls = (<*>) (fmap (,) gen_coord) $ vectorOf 4 gen_entity

gen_coord_ent :: Gen (Coord, [Entity]) 
gen_coord_ent = do 
    coord   <- gen_coord
    ent     <- vectorOf 2 gen_entity
    return $ (coord,ent)


data CorrectEnvi= CorrectEnvi {getCorrectEnvi :: Envi}
instance Arbitrary Entity where 
    arbitrary = gen_entity


instance Arbitrary Envi where
    arbitrary = build_env


build_env :: Gen Envi 
build_env = do 
    nb      <-  choose (50,300)
    build_map_entity nb

build_map_entity :: Int -> Gen Envi  
build_map_entity nb = build_ls_entity nb mempty

build_ls_entity :: Int -> Envi -> Gen Envi
build_ls_entity 0 env = return env
build_ls_entity nb old_env = do
    coord   <-  gen_coord
    ent     <-  gen_entity
    if (check_all_invariant_envi $ add_env coord ent old_env) == Right True then
        build_ls_entity (nb-1) (add_env coord ent old_env)
    else
        build_ls_entity nb old_env


--Extract id of a random player 
extract_id_player :: Envi -> Gen Int
extract_id_player env = 
    case (LS.find (\u -> isPlayer u) ls) of 
            Just Player{iden=id} -> return id
    where   ls = case (env) of 
                Envi a -> LS.foldr (\b acc -> b ++ acc) [] (Map.elems a)



--Extract one random id in our environment
extract_alea_id :: Envi -> Gen Int 
extract_alea_id Envi{content_envi=cont} = do
    ind_map     <-  choose (0,len-1)
    ind_list    <-  case (Map.elemAt ind_map cont) of 
        (_,a) -> choose (0, (LS.length a) -1)
    return $ case (Map.elemAt ind_map cont) of
        (_,v) -> extract_id_entity $ v !! ind_list   
    where   len = (Map.size cont)



--Add to environment a random id already existing into envi
add_same_id :: Envi -> Gen Envi
add_same_id env@Envi{content_envi=content} = do
    id  <-  extract_alea_id env
    coord <- gen_coord
    return $ add_env coord (Potion id Water) env


--generate environment to test different_id_inv invariant
gen_multiple_same_ids :: Gen Envi
gen_multiple_same_ids = do 
    env     <- build_env
    add_same_id env


add_empty_case :: Envi -> Gen Envi
add_empty_case env@Envi{content_envi=content} = do
    coord   <-  gen_coord
    return $ (Envi $ Map.insert coord [] content)

--generate empty case in our environment
gen_empty_case_envi :: Gen Envi
gen_empty_case_envi =  do
    env     <-  build_env
    add_empty_case env


--generate a cow in each case of our environment in order to have at least one case with multiple cows or one player and one Cow 
gen_multiple_cow :: Gen Envi
gen_multiple_cow = do
    env     <- build_env
    case (env) of
        Envi a -> return $ Envi $ Map.map (\v -> v++[(Cow 10 0 0 0 0 0)]) a --v ++ [gen_cow]) a

--generate multiple traps in some cases inside the environment
gen_multiple_trap :: Gen Envi 
gen_multiple_trap = do 
    env     <-  build_env
    case (env) of 
        Envi a -> return $ Envi $ Map.map (\v -> v++[(Trap 3 10),(Trap 4 10)]) a


gen_chests_around_player :: Gen Envi
gen_chests_around_player  = do
    env     <- build_env
    id      <- extract_id_player env
    chest   <- gen_chest
    case (find_id id env) of 
        Just(coord,_)     -> case coord of 
            (C x y)             -> return $ (LS.map (\a -> add_env a chest env) [(C (x-1) y),(C (x+1) y), (C x (y-1)), (C x (y+1))]) !! 0

--Inventory generating
gen_inventory :: Gen Inventory 
gen_inventory = do 
    nb      <-  choose (2,100)
    ls      <-  vectorOf nb $ oneof [gen_potion,gen_armor]
    return $ Inv 0 ls

gen_navigateInventory :: Gen NavigateInventory
gen_navigateInventory = let (left,right) = (pure LeftN, pure RightN) in oneof [left,right]--oneof $ return $ [LeftN,RightN]

buildCorrectEnvi = do 
    describe "all invariants" $ do 
        it "test all invariants" $ do 
            env      <-     generate build_env
            (check_all_invariant_envi env) `shouldBe` Right True


gridValidDifferentId = do
    describe "grid with multiple entities with same id" $ do 
        it "Test multiple ids invariant" $ do
            env <- generate gen_multiple_same_ids
            (different_id_inv env) `shouldBe` False


gridValidNoEmptyCases = do 
    describe "grid with empty list inside" $ do 
        it "Test no empty lists in the environment map" $ do 
            env <- generate gen_empty_case_envi
            (no_empty_case_env env) `shouldBe` False


gridValidMultipleCowsInSameCase = do 
    describe "grid with multiple cows in same case" $ do 
        it "Test multiple cows in same case in the environment map" $ do 
            env <- generate gen_multiple_cow
            (cases_inv env) `shouldBe` False

gridValidMultipleTrapsInSameCase = do
    describe "grid containing multiple traps in same case " $ do 
        it "Test multiple traps in same case inside environment" $ do 
            env <- generate gen_multiple_trap
            (uniqueTrap_inv env) `shouldBe` False

postconditionMvEnvId = do
    describe "postcondition move id correct" $ do 
        it "test postcondition move_id" $ do
            env     <-  generate build_env
            id      <-  generate $ extract_alea_id env
            coord   <-  generate $ suchThat gen_coord (\a -> (move_id_pre id a env)==True )
            (move_id_post id coord $ move_id id coord env) `shouldBe`  True


postconditionAddEnvId = do 
    describe "postcondition add id correct" $ do
        it "test postcondition add_id" $ do 
            env     <-  generate build_env
            coord   <-  generate gen_coord
            ent     <-  generate $ suchThat gen_entity (\a -> (add_env_pre coord a env)==True)
            (add_env_post coord ent $ add_env coord ent env) `shouldBe` True


postconditionRmEnvId = do
    describe "postcondition remove environment id correct" $ do 
        it "test postcondition rm_env_id" $ do 
            env <-  generate build_env
            id  <-  generate $ suchThat (choose (0,1000)) (\a -> (rm_env_id_pre a env)==True)
            (rm_env_id_post id $ rm_env_id id env) `shouldBe` True


postconditioninteractEnvi2 = do 
    describe "postcondition interact environment" $ do 
        it "test postcondition interactEnvi2_post_chest" $ do 
            env     <-  generate gen_chests_around_player
            id      <-  generate $ extract_id_player env
            case (find_id id env) of 
                Just (coord,ent) -> (interactEnvi2_post_chest id (interactEnvi2 id env)) `shouldBe` True
            

postconditionswitchEquip = do 
    describe "postcondition switch equipment" $ do 
        it "Test post-condition switchEquip_post" $ do 
            env     <-  generate build_env
            id      <-  generate $ extract_id_player env
            arm     <-  generate $ gen_equipment
            case (find_id id env) of
                Just (_,player) -> (switchEquip_post (switchEquip player arm ) arm) `shouldBe` True

postconditionhealPlayer = do 
    describe "post-condition heal player" $ do 
        it "Test post-condition healPlayer_post" $ do 
            player  <-  generate gen_player
            nb      <-  generate $ choose (1,10)
            (healPlayer_post $ healPlayer player nb) `shouldBe` True

postconditiondrinkPlayer = do 
    describe "post-condition drink player" $ do 
        it "Test post-condition drinkWater_post" $ do 
            player  <-  generate gen_player 
            nb      <-  generate $ choose (1,10)
            (drinkWater_post (drinkWater player nb)) `shouldBe` True 


postconditiondrinkPotion = do 
    describe "post-condition drink potion player" $ do 
        it "Test post-condition drinkPotion_post" $ do 
            player  <-  generate gen_player
            potion  <-  generate gen_potion
            case player of 
                Player{inv=inventory} -> (drinkPotion_post potion (drinkPotion player{inv=(addToInventory inventory potion)} potion)) `shouldBe` True

-- Inventory tests
inventoryValidInvariants = do 
    describe "Check all invariants of an inventory" $ do 
        it "Test post-condition correct_index_inventory" $ do 
            inventory   <- generate gen_inventory
            (correct_index_inventory inventory) `shouldBe` True 

postconditionnavigateInventory = do
    describe "post-condition navigate inventory" $ do 
        it "post-condition navigate inventory" $ do
            inventory   <-  generate gen_inventory
            way         <-  generate gen_navigateInventory
            (navigateInventory_post inventory (navigateInventory inventory way)) `shouldBe` True


postconditionnaddToInventory = do 
    describe "post-condition add to inventory" $ do 
        it "post-condition addToInventory" $ do 
            inventory   <-  generate gen_inventory
            pickable    <-  generate gen_pickable
            (addToInventory_post (addToInventory inventory pickable) pickable) `shouldBe` True

postconditionremoveFromInventory = do 
    describe "post-condition remove from inventory" $ do 
        it "post-condition removeFromInventory" $ do 
            inventory   <-  generate gen_inventory
            let item = ((content_inventory inventory) !! 0) in (removeFromInventory_post (removeFromInventory inventory item) item ) `shouldBe` True 

totalEnviCheck = do 
    buildCorrectEnvi
    gridValidDifferentId
    gridValidNoEmptyCases
    gridValidMultipleCowsInSameCase
    gridValidMultipleTrapsInSameCase
    postconditionMvEnvId
    postconditionAddEnvId 
    postconditionRmEnvId
    postconditioninteractEnvi2
    postconditionswitchEquip
    postconditionhealPlayer
    postconditiondrinkPlayer
    postconditiondrinkPotion
    inventoryValidInvariants
    postconditionnavigateInventory
    postconditionnaddToInventory
    postconditionremoveFromInventory