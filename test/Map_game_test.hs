module Map_game_test where
import Map_game
import Test.QuickCheck
import Test.Hspec
import qualified Data.Map as Map


--instance Semigroup (Gen a) where
--    (<>) x y =
--        do 
--            str1 <- generate x
--            str2 <- generate y
--           putStrLn (elements (str1<>str2))



sample1 = extractMyMapFromRead "XXXX\nXXX" --grid not full
sample2 = extractMyMapFromRead ""           -- empty grid
sample3 = extractMyMapFromRead "XXXX\nXEXX\nXXXX" -- exit missing
sample4 = extractMyMapFromRead "XXXX\nXEXX\nXXSX\nXEXX\nXXXX" -- 2 enters
sample5 = extractMyMapFromRead "XXXX\nXSXX\nXXSX\nXEXX\nXXXX" -- 2 exits
sample6 = extractMyMapFromRead "XXX \nXE|X\nXXXX" -- grid not surrounded by walls
sample7 = (Map_game (-1) 0 Map.empty) :: Map_game-- not good dimension
sample8 = extractMyMapFromRead "XXXX\nX|-X\nXXXX" -- horizontal door not between walls
sample9 = extractMyMapFromRead "XXXX\nX  X\nX|XX\nXX-X" -- vertical door not between walls
sample10 = extractMyMapFromRead "XXXX\nXXEX\nXXXX\nXSXX\nXXXX" -- not valid path leading to the exit
sample11 = extractMyMapFromRead "XXXX\nX  X\nXEXX\nXSXX" -- correct
sample12 = extractMyMapFromRead "XXXXX\nX-X|X\nX  XX\nXE SX\nXXXXX"

gridValidPathSpec = do 
    describe "grid with no valid path" $ do
        it "Test no valid path" $ do
            (check_path_exist_inv sample10)
            `shouldBe` False
gridValidPathSpec2 = do
    describe "grid with valid path" $ do
        it "Test valid path" $ do
            (check_path_exist_inv sample11)
            `shouldBe` True
gridCheckAllSpec = do
    describe "Correct grid" $ do
        it "Test all properties correct" $ do
            (check_all_invariant_map_game sample12) 
            `shouldBe` (Right True)

gridFullSpec = do
    describe "grid not full" $ do
        it "Test full" $ do
            (map_full_with_elements_inv sample1)
            `shouldBe` False

gridOwnUniqueEntrySpec = do
    describe "grid with one entry" $ do
        it "check grid one entry" $ do 
            (check_map_contains_entry_inv sample3)
            `shouldBe` True

gridOwnUniqueExitSpec = do
    describe "grid without Exit" $ do
        it "test one Exit False" $ do 
            (check_map_contains_exit_inv sample3)
            `shouldBe` False

gridMultipleEntrySpec = do
    describe "grid multiple entries" $ do
        it "test unique Entry" $ do
            (check_map_contains_entry_inv sample4)
            `shouldBe` False
gridMultipleExitSpec = do
    describe "grid multiple exits" $ do
        it "test unique exit" $ do
            (check_map_contains_exit_inv sample5)
            `shouldBe` False

gridSurroundedSpec = do
    describe "grid not surrounded" $ do 
        it "test grid surrounded" $ do 
            (check_map_surrounded_by_walls_inv sample6)
            `shouldBe` False
    
gridCorrectDimensionSpec = do
    describe "grid incorrect dimension" $ do 
        it "test incorrect dimesnion" $ do
            (map_correct_dimension_inv sample7)
            `shouldBe` False
horizontalDoorSurroundedByWallsSpec = do
    describe "Vertical doors surrounded by walls but not horizontal" $ do
        it "doors between walls" $ do
            (check_doors_between_walls_inv sample8) 
            `shouldBe` False
verticalDoorSurroundedByWallsSpec = do
    describe "Horizontal doors surrounded by walls but not verticals" $ do
        it "doors between walls" $ do
            (check_doors_between_walls_inv sample9) 
            `shouldBe` False
doorsSurroundedByWallsSpec  = do
    describe "grid with one horizontal door and one vertical door" $ do 
        it "doors between walls" $ do
            (check_doors_between_walls_inv sample12)
            `shouldBe` True

totalMapCheck = do
    gridFullSpec
    gridOwnUniqueEntrySpec
    gridOwnUniqueExitSpec
    gridMultipleEntrySpec
    gridMultipleExitSpec
    gridSurroundedSpec
    gridCorrectDimensionSpec
    horizontalDoorSurroundedByWallsSpec
    verticalDoorSurroundedByWallsSpec
    doorsSurroundedByWallsSpec
    gridValidPathSpec
    gridValidPathSpec2
    gridCheckAllSpec
