module Model where
import SDL 

import Entity 
import Map_game
import Environment 
import System.Random
import System.Directory
import Keyboard 
import Equipment 

import qualified Keyboard as K
import qualified Data.Map as Map
import qualified Data.List as LS
import Control.Concurrent (threadDelay)

import PlaySound

data Model = Cont {
                    grid :: Map_game,
                    envi :: Envi,
                    gene :: StdGen,
                    logu :: String,
                    keyboard :: Keyboard,
                    timestmp :: Int
                    }

initModel :: Model 
initModel = Cont init_map_game init_map_envi (mkStdGen 3) "" K.createKeyboard 0

init_map_game :: Map_game 
init_map_game = extractMyMapFromRead "XXXXXXXX\nXE  XXXX\nX      X\nX   X  X\nX   XXXX\nX   X  X\nX   | SX\nXXXXXXXX" -- extractMyMapFromRead "XXXXXX\nXE   X\nXX-XXX\nX   SX\nX  XXX\nXXXXXX"

init_map_envi :: Envi 
init_map_envi = add_env (C 1 1) (Player 1 1000 1000 10 10 10 10 mempty mempty 100 100 0) $
                add_env (C 3 2) (Gobelin 6 10 10 10 10 10 0) $
                add_env (C 6 2) (Chest 10 [(Key 11),
                                                    (SetArmor $ (Gloves 17 10 10 10)),
                                                    (SetArmor $ (Sword 18 20)),
                                                    (SetArmor $ (Boots 15 10 10 10 10)),
                                                    (SetArmor $ (Helmet 16 10 10 10 10)),
                                                    (SetArmor $ (Chest_armor 14 100 10 10 10)),
                                                    (SetArmor $ (Legs 19 10 10 10)),
                                                    (Potion 13 Health),
                                                    (Potion 20 Water)]) $
                add_env (C 3 3) (Gobelin 7 10 10 10 10 10 0) $
                add_env (C 1 6) (Gobelin 8 10 10 10 10 10 0) $
                add_env (C 5 5) (Troll 9 20 20 10 3 10 0) $ 
                add_env (C 3 4) (Trap 12 10) $
                mempty
        

    --add_env (C 4 1) (Chest 10 [(Key 11),(SetArmor $ (Helmet 14 10 10 10 10))]) $ add_env (C 2 2) (Gobelin 6 10 10 10 10 10 0) $ add_env (C 2 1) (Gobelin 3 10 10 10 10 10 0) $ add_env (C 1 3) (Gobelin 4 10 10 10 10 10 0) $ add_env (C 1 2) (Troll 5 10 10 10 30 10 0) $ add_env (C 1 1) (Player 1 1000 1000 10 1000 10 10 mempty mempty 10 10 0) mempty--add_env (C 1 3) (Gobelin 11 10 10 10 10 10) $ add_env (C 3 4) (Gobelin 6 10 10 10 10 10) $ add_env (C 3 3) (Gobelin 5 10 10 10 10 10) $ add_env (C 1 2) (Cow 12 10 10 10 10) $ add_env (C 2 2) (Chest 20 [(Key 99),(Potion 34 Health)]) $ add_env (C 2 1) (Potion 10 Water) $ add_env (C 1 1) (Player 1 1000 1000 10 10 10 10 mempty mempty 10 10) mempty --add_env (C 2 2) (Cow 2 10 10 10 10 ) $ add_env (C 1 1) (Player 1 1000 1000 10 10 10 10 mempty mempty 10 10) mempty--add_env (C 2 1) (Gobelin 3 10 10 10 10 10) $ add_env (C 1 3) (Gobelin 4 10 10 10 10 10) $ add_env (C 1 2) (Gobelin 5 10 10 10 10 10) $ add_env (C 1 1) (Player 1 1000 1000 10 10 10 10 mempty mempty 10 10) mempty--add_env (C 1 3) (Gobelin 11 10 10 10 10 10) $ add_env (C 3 4) (Gobelin 6 10 10 10 10 10) $ add_env (C 3 3) (Gobelin 5 10 10 10 10 10) $ add_env (C 1 2) (Cow 12 10 10 10 10) $ add_env (C 2 2) (Chest 20 [(Key 99),(Potion 34 Health)]) $ add_env (C 2 1) (Potion 10 Water) $ add_env (C 1 1) (Player 1 1000 1000 10 10 10 10 mempty mempty 10 10) mempty

move :: Model -> Entity -> Coord -> Model 
move mod@Cont{grid=gridd, envi=env, logu=logg} ent coord
    |   (check_all_invariant_envi env) /= (Right True)              = mod {logu="\n moved unsuccessfully all invariants not true for envi"++(show $ check_all_invariant_envi env)}
    |   (check_all_invariant_map_game gridd) /= (Right True)        = mod {logu="\n moved unsuccessfully all invariants not true for map_game"++(show $ check_all_invariant_map_game gridd)}
    |   (fordable_env coord env) == False                           = mod {logu="\n moved unsuccessfully to this coord due to environment"}
    |   (fordable_case_map_game gridd False coord)==False           = mod {logu="\n moved unsuccessfully this coord in the static grid cannot be reach"}
    |   (move_id_pre id coord env) == False                         = mod {logu="\n moved unsuccessfully false precondition"}
    |   (move_id_pre_correct_coord gridd coord) == False            = mod {logu="\n moved unsuccessfully you cannot access this cord, not in the grid"}
    |   (mapValidation mod) == False                                = mod {logu="\n moved unsuccessfully mapValidation not correct"}
    |   otherwise                                                   = mod {envi=(move_id id coord env), logu="\n moved successfully"} --can add something in the log 
    where   id = extract_id_entity ent


moveUp :: Model -> Entity -> Model
moveUp mod@Cont{envi=env} ent =
    move mod ent coord
    where   id      = extract_id_entity ent
            coord   = case (find_id id env) of 
                Just ((C x y),_)  -> (C x (y-1))
                
moveDown :: Model -> Entity -> Model
moveDown mod@Cont{envi=env} ent =
    move mod ent coord
    where   id      = extract_id_entity ent
            coord   = case (find_id id env) of 
                Just ((C x y),_)  -> (C x (y+1))

moveRight :: Model -> Entity -> Model
moveRight mod@Cont{envi=env} ent =
    move mod ent coord
    where   id      = extract_id_entity ent
            coord   = case (find_id id env) of 
                Just ((C x y),_)  -> (C (x+1) y)
    
moveLeft :: Model -> Entity -> Model
moveLeft mod@Cont{envi=env} ent =
    move mod ent coord
    where   id      = extract_id_entity ent
            coord   = case (find_id id env) of 
                Just ((C x y),_)  -> (C (x-1) y)

attack :: Model -> Entity -> Model 
attack mod@Cont{grid=gridd, envi=env, logu=logg} ent 
    | (attack_id_pre id env)    == False            = mod {logu=logg++"\n attacked unsuccessfully false precondition cannot attack"}
    | (attack_id_post env)      == False            = mod {logu=logg++"\n attacked unsuccessfully false postcondition cannot remove dead characters"}
    | otherwise                                     = mod {envi=uncrementThirst_id_player id 5 $ (attack_id id env), logu=logg++"\n attacked successfully"}
    where   id = extract_id_entity ent 

interactModel :: Model -> Entity -> Model 
interactModel mod@Cont{envi=env, logu=logg} ent 
    | (entity_player_pre ent) == False                                  = mod {logu=logg++"\n interact unsuccessfully false precondition: Only a player can interact"}
    | (interactEnvi2_post_chest id $ (interactEnvi2 id env)) == False   = mod {logu=logg++"\n interact unsuccessfully false postcondition: chest still around player"}
    | otherwise                                                         = mod {envi=(interactEnvi2 id env),logu=logg++"\n Player interacted successfully with its environment"}
    where   id = extract_id_entity ent

useModel :: Model -> Entity -> Model 
useModel mod@Cont{envi=env, logu=logg, grid=gridd} ent 
    | (entity_player_pre ent) == False               = mod {logu=logg++"\n use unsuccessfully false precondition: Only player can use their inventory"}
    | otherwise                                      = mod {envi=newEnv, grid=newMap, logu=logg++"\n used successfully"}
    where   (newEnv,newMap) = useEnv env ent gridd --It has to be lazy to not trigger error


gameStep :: RealFrac a => Model -> Keyboard -> a -> (String,Model)
gameStep model@Cont{envi=env,timestmp=time} kbd deltaTime
    | cd < (floor $ (fromIntegral 100 / fromIntegral sp))       =   (mempty, model{envi=(add_env coord_player (player_entity{coolDown=(cd+1)}) $ (rm_env_id (iden player_entity) env))})
    | (K.keypressed (KeycodeO) kbd)                             =   (mempty, let t = threadDelay $ 5000 in model{envi=add_env coord_player (rollInventory new_player_entity RightN) $ (rm_env_id (iden player_entity) env)})
    | (K.keypressed (KeycodeP) kbd)                             =   (mempty, let t = threadDelay $ 5000 in model{envi=add_env coord_player (rollInventory new_player_entity LeftN) $ (rm_env_id (iden player_entity) env)})
    | (K.keypressed (KeycodeW) kbd)                             =   ("stepSound", let mo = move (modifymodelcooldown model) new_player_entity (C x_player (y_player-1)) in mo{envi=uncrementThirst_id_player (iden player_entity) 1 (envi mo)})
    | (K.keypressed (KeycodeS) kbd)                             =   ("stepSound", let mo = move (modifymodelcooldown model) new_player_entity (C x_player (y_player+1)) in mo{envi=uncrementThirst_id_player (iden player_entity) 1 (envi mo)})
    | (K.keypressed (KeycodeA) kbd)                             =   ("stepSound", let mo = move (modifymodelcooldown model) new_player_entity (C (x_player-1) y_player) in mo{envi=uncrementThirst_id_player (iden player_entity) 1 (envi mo)})
    | (K.keypressed (KeycodeD) kbd)                             =   ("stepSound", let mo = move (modifymodelcooldown model) new_player_entity (C (x_player+1) y_player) in mo{envi=uncrementThirst_id_player (iden player_entity) 1 (envi mo)})
    | (K.keypressed (KeycodeE) kbd)                             =   (useSound, let t = threadDelay $ 5000 in (useModel (modifymodelcooldown model) player_entity)) 
    | (K.keypressed (KeycodeT) kbd)                             =   ("interact", (interactModel (modifymodelcooldown model) new_player_entity)) 
    | (K.keypressed (KeycodeSpace) kbd)                         =   ("attack", (attack (modifymodelcooldown model) new_player_entity))
    |  otherwise                                                =   (mempty, model)
    where   (coord_player,player_entity) = get_first_player_from_envi env 
            (x_player,y_player)          = case (coord_player) of (C x y) -> (x,y)
            sp                           = speed player_entity
            cd                           = coolDown player_entity
            new_player_entity = player_entity{coolDown=0}
            modifymodelcooldown m = m{envi=(add_env coord_player new_player_entity $ (rm_env_id (iden player_entity) env))}
            useSound    =   case (getCurrentItem $ inv player_entity) of 
                                Just Key{}       -> "keySound" 
                                Just Potion{}    -> "drinkSound"
                                Just SetArmor{}  -> "equipmentSound"
                                Nothing          -> ""


-- Trying to solve the problem concerning the invariant path exists considering a map and an Environment

{-
    if a map_game without any closed door True 
    else one door
        if it is still possible to join the exit from then entry without being blocked by any door then -> True
        else 
            if one chest with a key among other things is reachable from the entry -> True 
            else -> False
-}
-- check if the map is correct
mapValidation :: Model -> Bool
mapValidation Cont{envi=env, grid=gridd} = 
    case (check_path_exist_inv gridd) of -- 
                True -> True -- a path exists with no closed door
                False -> case (check_path_exist_inv_key gridd) of --may be an impossible map or a closed door on the path 
                    True    -> correctChestOnPath_inv gridd env -- closed door on the path need a key 
                    False   -> False -- impossible map because no chest 


finished_game :: Model -> Bool 
finished_game gameState@Cont{envi=env, grid=gridd}=
    case (get_first_player_from_envi env) of 
        (coord_player, player_entity@Player{pvie=pv}) -> 
            if (pv<=0) then 
                True 
            else 
                case (look_case_map_game gridd coord_player) of 
                    Just Exit -> True 
                    _         -> False

