module Map_game 
    where

import qualified Data.Map as Map
import qualified Data.List as LS

data DDirection = NS | EW deriving Eq --do tests about post conditions after player did something

data DState = Opened | Closed deriving Eq

data Case = Empty_case
    | Door DDirection DState
    | Wall 
    | Enter
    | Exit 
    deriving Eq

data Coord = C {cx :: Int , cy :: Int} deriving Eq

data Error = Error String 
    deriving (Show,Eq)


keyCodeCaseName::Case -> Char
keyCodeCaseName c | c == Empty_case || c == (Door NS Opened) || c == (Door EW Opened)   = ' '                                   
keyCodeCaseName Wall                                                                    = 'X'
keyCodeCaseName Enter                                                                   = 'E'
keyCodeCaseName Exit                                                                    = 'S'
keyCodeCaseName c | c == (Door NS Closed)                                               = '|'
keyCodeCaseName c | c == (Door EW Closed)                                               = '-'
keyCodeCaseName _                                                                       = ' '  -- opened door displayed as empty_case 



instance Show (Case) where
    show c = show(keyCodeCaseName c)

data Map_game = Map_game {
                        map_gw :: Int,--width
                        map_gh :: Int, --height
                        map_content :: (Map.Map Coord Case) -- content's cases of map_game
                    }


instance Ord (Coord) where
    (<=) (C x1 y1) (C x2 y2) | x1 < x2  = True
        | x1==x2   &&   y1 < y2         = True
        | x1==x2   &&   y1 == y2        = True
        | otherwise                     = False
    
instance Show (Map_game) where
    show m = case (show_map_game m) of
        Left (Error str) -> str
        Right b -> b 

instance Show Coord where
    show (C a b) = "(" <> (show a) <> "," <> (show b) <> ")" 

instance Semigroup Map_game where
    (<>) Map_game{map_gh=height1, map_gw=width1, map_content=mapg1} Map_game{map_gh=height2, map_gw=width2, map_content=mapg2} = Map_game (max width1 width2) (max height1 height2) (Map.union mapg1 mapg2)

instance Monoid Map_game where
    mempty = Map_game 0 0 (Map.empty)


instance Read Map_game where 
    readsPrec _ str = [(parsecRead str, "")]

instance Eq Map_game where
    (==) Map_game{map_gh=height1, map_gw=width1, map_content=mapg1} Map_game{map_gh=height2, map_gw=width2, map_content=mapg2} = (width1==width2) && (height1==height2) && (mapg1==mapg2)


extractMyMapFromRead :: String -> Map_game
extractMyMapFromRead str = case (readsPrec 0 str) of 
    (x:_) -> case x of
        (map,_) -> map 

parsecRead :: String -> Map_game
parsecRead str = build_Map_from_string 0 0 str

build_Map_from_string :: Int -> Int -> String -> Map_game
build_Map_from_string _ _ []    = mempty
build_Map_from_string w h (x:xs) 
    | x=='X'                    = r w h Wall <> build_Map_from_string (w+1) h xs
    | x=='\n'                   = build_Map_from_string (0) (h+1) xs
    | x=='E'                    = r w h Enter <> build_Map_from_string (w+1) h xs
    | x=='S'                    = r w h Exit <> build_Map_from_string (w+1) h xs
    | x=='-'                    = r w h (Door EW Closed) <> build_Map_from_string (w+1) h xs
    | x=='|'                    = r w h (Door NS Closed) <> build_Map_from_string (w+1) h xs
    | x==' '                    = r w h (Empty_case) <> build_Map_from_string (w+1) h xs
    | otherwise                 = mempty
        where r a b c = (Map_game (a+1) (b+1) (Map.insert (C a b) c Map.empty))



all_element_inside_grid_inv :: Map_game -> Bool -- Check whether or not all elements are contained in the grid 
all_element_inside_grid_inv Map_game{map_gw=width, map_gh=height, map_content=mapg} = Map.foldrWithKey (\ (C x1 y1) _ b -> ((x1<width && y1<height) && b) ) True mapg

map_full_with_elements_inv :: Map_game -> Bool -- Check whether or not all case in the grid exists in the map 
map_full_with_elements_inv Map_game{map_gw=width, map_gh=height, map_content=mapg} = (Map.size mapg) == (width * height)

map_correct_dimension_inv :: Map_game -> Bool -- no negative dimensions
map_correct_dimension_inv Map_game{map_gw=width, map_gh=height, map_content=mapg} 
    | width >= 0 && height >= 0     = True
    | otherwise                     = False

check_map_contains_entry_inv :: Map_game -> Bool -- check whether or not our map contains a unique entry
check_map_contains_entry_inv Map_game{map_content=mapg} = (Map.foldr (\val acc -> if (val==Enter) then acc+1 else acc) 0 mapg) == 1

check_map_contains_exit_inv :: Map_game -> Bool -- check whether or not our map contains a unique exit
check_map_contains_exit_inv Map_game{map_content=mapg} = (Map.foldr (\val acc -> if (val==Exit) then acc+1 else acc) 0 mapg)   == 1

check_map_surrounded_by_walls_inv :: Map_game -> Bool --check whether or not our map is surrounded by wall 
check_map_surrounded_by_walls_inv my_map@Map_game{map_gw=width, map_gh=height, map_content=mapg} = (c_check 0 (height-1)) && (c_check (width-1) (height-1)) && (l_check (width-1) 0) && (l_check (width-1) (height-1))
    where l_check = (check_map_lines_walls my_map )
          c_check = (check_map_columns_walls my_map)

check_map_columns_walls :: Map_game -> Int -> Int -> Bool --check walls columns wise
check_map_columns_walls my_map@Map_game{map_gw=width, map_gh=height, map_content=mapg} column line 
    | line == 0 = w == Wall 
    | otherwise = w == Wall && (check_map_columns_walls my_map column (line-1))
        where w = case (Map.lookup (C column line) mapg) of Just a -> a 

check_map_lines_walls :: Map_game -> Int -> Int -> Bool -- check walls lines wise
check_map_lines_walls my_map@Map_game{map_gw=width, map_gh=height, map_content=mapg} column line 
    | column == 0 = w == Wall
    | otherwise = w == Wall && (check_map_lines_walls my_map (column-1) line)
        where w = case (Map.lookup (C column line) mapg) of Just a -> a


check_doors_between_walls_inv :: Map_game -> Bool -- check Doors surrouned by walls 
check_doors_between_walls_inv Map_game{map_content=mapg} = Map.foldrWithKey (\k v acc -> (check_door_between_walls_unit mapg k v) && acc) True mapg

check_door_between_walls_unit :: Map.Map Coord Case -> Coord -> Case -> Bool
check_door_between_walls_unit mapg (C x y) v 
    | v==(Door NS Opened) || v == (Door NS Closed)                                                   = ((w x (y-1)) == Wall) && ((w x (y+1)) == Wall)
    | v==(Door EW Opened) || v == (Door EW Closed)                                                   = ((w (x-1) y) == Wall) && ((w (x+1) y) == Wall)
    | v==(Door EW Opened) || v == (Door EW Closed) || v == (Door NS Opened) || v == (Door NS Closed) = False
    | otherwise                                                                                      = True
        where w a b = case (Map.lookup (C a b) mapg) of 
                Just u -> u
                Nothing -> Empty_case
            
-- Take a Map_game and return True if the player at an entry can reach Exit with no key
check_path_exist_inv :: Map_game -> Bool
check_path_exist_inv m = (check_path_exist_unit_2 False m w []) 
        where   w = case (find_entry_from_map_game m) of 
                    (Just a) -> a

-- Take a Map_game and return True if the player at an entry can reach the exit knowing he got all keys
check_path_exist_inv_key :: Map_game -> Bool
check_path_exist_inv_key m = (check_path_exist_unit_2 True m w []) 
        where   w = case (find_entry_from_map_game m) of 
                    (Just a) -> a



find_entry_from_map_game :: Map_game -> Maybe Coord -- Should not return Nothing if check_map_contains_entry_inv has been already checked
find_entry_from_map_game Map_game{map_content=mapg} = Map.foldrWithKey (\k v acc -> myMaybe_applicative mapg k v acc) Nothing mapg

myMaybe_applicative :: Map.Map Coord Case -> Coord -> Case -> Maybe Coord -> Maybe Coord -- Useful to keep the adequate coord of the entry 
myMaybe_applicative my_map k v acc 
    | (Map.lookup k my_map) == (Just Enter)     = Just k
    | otherwise                                 = acc

check_path_exist_unit_2 :: Bool -> Map_game -> Coord -> [Coord] -> Bool
check_path_exist_unit_2 True my_map@Map_game{map_gw=width, map_gh=height, map_content=mapg} (C x y) ls
    | (x > (width-1)) || x < 0 || y < 0 || y > (height-1)                                                   = False
    | (LS.elem (C x y) ls) == True                                                                          = False -- coord already checked
    | (current_item == Wall)                                                                                = False
    | current_item == Exit                                                                                  = True
    | otherwise                                                                                             = (gm (x-1) y) || (gm (x+1) y) || (gm (x) (y-1)) || (gm (x) (y+1))
        where   current_item  = case (Map.lookup (C x y) mapg) of Just u -> u
                new_ls        = LS.insert (C x y) ls
                gm a b        = check_path_exist_unit_2 True my_map (C a b) new_ls
check_path_exist_unit_2 False my_map@Map_game{map_gw=width, map_gh=height, map_content=mapg} (C x y) ls
    | (x > (width-1)) || x < 0 || y < 0 || y > (height-1)                                                   = False
    | (LS.elem (C x y) ls) == True                                                                          = False -- coord already checked
    | (current_item == Wall) || (current_item == (Door EW Closed)) || (current_item == (Door NS Closed))    = False
    | current_item == Exit                                                                                  = True
    | otherwise                                                                                             = (fm (x-1) y) || (fm (x+1) y) || (fm (x) (y-1)) || (fm (x) (y+1))
        where   current_item  = case (Map.lookup (C x y) mapg) of Just u -> u
                new_ls        = LS.insert (C x y) ls
                fm a b        = check_path_exist_unit_2 False my_map (C a b) new_ls



check_all_invariant_map_game :: Map_game -> Either Error Bool
check_all_invariant_map_game m
    | (all_element_inside_grid_inv m) == False          = Left (Error "Error invariant: all elements must be inside the grid")
    | (map_full_with_elements_inv m) == False           = Left (Error "Error invariant: The grid must be full")
    | (map_correct_dimension_inv m) == False            = Left (Error "Error invariant: Bad dimension of the grid")
    | (check_map_surrounded_by_walls_inv m) == False    = Left (Error "Error invariant: The map must be surrounded by walls")
    | (check_map_contains_entry_inv m) == False         = Left (Error "Error invariant: The map must contains a unique Entry")
    | (check_map_contains_exit_inv m) == False          = Left (Error "Error invariant: The map must contains a unique exit")
    | (check_doors_between_walls_inv m) == False        = Left (Error "Error invariant: Each door of the map must be surrounded by walls")
  --  | (check_path_exist_inv m) == False                 = Left (Error "Error invariant: Incorrect map, A path must exist between entry and Exit")
    | otherwise                                         = Right True

show_map_game :: Map_game -> Either Error String
show_map_game my_map@Map_game{map_gw=width, map_gh=height, map_content=mapg} 
    = case (check_all_invariant_map_game my_map) of
        Left (Error a) -> Left (Error a)
        Right b -> Right (case (show_map_game_int my_map (height-1) (width-1) ) of (Just a) -> a)

show_map_game_int :: Map_game -> Int -> Int -> Maybe String
show_map_game_int _ l c | l<0 || c<0                                                    = Nothing
show_map_game_int Map_game{map_content=mapg} 0 0                                        = show_case_map_game mapg 0 0 
show_map_game_int my_map@Map_game{map_gh=height, map_gw=width, map_content=mapg} l 0    = (show_map_game_int my_map (l-1) (width-1)) <> (Just "\n") <> (show_case_map_game mapg l 0) 
show_map_game_int my_map@Map_game{map_gw=width, map_gh=height, map_content=mapg} l c    = (show_map_game_int my_map l (c-1) ) <> (show_case_map_game mapg l c) 

show_case_map_game :: Map.Map Coord Case -> Int -> Int -> Maybe String --Maybe safer
show_case_map_game mapg line col 
    | (Map.member (C col line) mapg) == True = 
        case (Map.lookup (C col line) mapg) of (Just c) -> Just [(keyCodeCaseName c)]
    | otherwise = Nothing-- The case is not in the map



-- OPERATIONS 

--Precondition: prop_coord_valid_pre
--Post-condition prop_case_coord_valid_post
look_case_map_game :: Map_game -> Coord -> Maybe Case 
look_case_map_game m@Map_game{map_content=mapg} coord = (Map.lookup coord mapg)
    -- | (prop_coord_valid_pre m coord) == True    = (Map.lookup coord mapg)
    -- | otherwise                                 = Nothing

-- check coord valid
prop_coord_valid_pre ::  Map_game -> Coord -> Bool
prop_coord_valid_pre Map_game{map_gh=height, map_gw=width, map_content=mapg} (C x y) 
    | width >= x || height >= y || x < 0 || y < 0 || (Map.member (C x y) mapg) == False = False
    | otherwise                                                                         = True                                                 
--check case valid
prop_case_coord_valid_post :: Maybe Case -> Bool
prop_case_coord_valid_post (Just c) = True
prop_case_coord_valid_post Nothing  = False


-- Check if a case is fordable

--Precondition: prop_coord_valid_pre
--Post-condition: prop_post_fordable_case_map_game 
--Second argument is a boolean indicating whether or not the player can open all doors
fordable_case_map_game :: Map_game -> Bool -> Coord -> Bool
fordable_case_map_game m True coord = case (look_case_map_game m coord) of 
    Just Wall               -> False
    Just Empty_case         -> True
    Just Exit               -> True 
    Just Enter              -> True 
    Just (Door _ Opened)    -> True
    _                       -> False
fordable_case_map_game m False coord = case (look_case_map_game m coord) of
    Just (Door _ Opened)    -> True
    Just Empty_case         -> True
    Just Enter              -> True 
    Just Exit               -> True 
    _                       -> False
-- Result should be true if the map did not change    
prop_post_fordable_case_map_game :: Map_game -> Map_game -> Bool
prop_post_fordable_case_map_game pre post = pre==post

--Doors operations
--open door if a closed door exists in coord
--Precondition prop_openDoor_exist_pre
--Precondition player got the key
openDoor :: Map_game -> Coord -> Map_game
openDoor Map_game{map_gh=height, map_gw=width,map_content=mapg} coord  = Map_game width height (Map.update (\c -> f c) coord mapg)  
    where   f a
                | a == (Door EW Closed) = Just (Door EW Opened)
                | a == (Door NS Closed) = Just (Door NS Opened)
                | otherwise             = Just a

--Pre-condition: check if a closed door exist in coord
prop_openDoor_exist_pre :: Map_game -> Coord -> Bool
prop_openDoor_exist_pre Map_game{map_content=mapg} coord = 
    case (Map.lookup coord mapg) of 
        Just a -> a == (Door NS Closed) || a == (Door EW Closed)
        _      -> False

--Post-condition: check if the door is opened
prop_openDoor_opened_post :: Map_game -> Coord -> Bool
prop_openDoor_opened_post m c =
     case (look_case_map_game m c) of 
        Just (Door EW Opened) -> True
        Just (Door NS Opened) -> True
        _                     -> False

-- CloseDoor
--Pre-condition: 
--Post-condition : prop_closeDoor_opened_po
closeDoor :: Map_game -> Coord -> Map_game
closeDoor Map_game{map_gh=height, map_gw=width,map_content=mapg} coord  = Map_game width height (Map.update (\c -> f c) coord mapg)  
    where   f a
                | a == (Door EW Opened) = Just (Door EW Closed)
                | a == (Door NS Opened) = Just (Door NS Closed)
                | otherwise             = Nothing


--check if an opened door exist in coord
prop_closeDoor_exist_pre :: Map_game -> Coord -> Bool
prop_closeDoor_exist_pre Map_game{map_content=mapg} coord = 
    case (Map.lookup coord mapg) of 
        Just a -> a == (Door NS Opened) || a == (Door EW Opened)
        _      -> False

--check if the door at the coord is closed
prop_closeDoor_closed_post :: Map_game -> Coord -> Bool
prop_closeDoor_closed_post m c =
     case (look_case_map_game m c) of 
        Just (Door EW Closed) -> True
        Just (Door NS Closed) -> True
        _                     -> False

--editCase edit a case
--precondition prop_coord_valid_pre + differentCaseProp_pre
editCase :: Map_game -> Coord -> Case -> Map_game
editCase m@Map_game{map_gh=height, map_gw=width,map_content=mapg} coord val= Map_game width height (Map.insert coord val mapg )
--Return True if the two cases are differents 
differentCaseProp_pre:: Map_game -> Coord -> Case -> Bool
differentCaseProp_pre m coord v = 
    case (look_case_map_game m coord) of 
        Just a  -> a /= v
        _       -> False


getAdjCases :: Coord -> [Coord]
getAdjCases (C x y) = [(C (x-1) y), (C (x+1) y), (C x (y-1)), (C x (y+1))]


containsClosedDoors :: Map_game -> Coord -> Maybe Coord
containsClosedDoors map@Map_game{map_content=content} coord = 
    (LS.find (\a -> isClosedDoors $ Map.lookup a content) ls_adj)
    where   ls_adj = getAdjCases coord


isClosedDoors :: Maybe Case -> Bool
isClosedDoors (Just (Door _ Closed))    = True
isClosedDoors _                         = False

someFunc :: IO ()
someFunc = putStrLn "someFunc"
