module Environment where
import Entity
import Map_game
import Equipment
import qualified Data.Map as Map
import qualified Data.List as LS
import Data.Set (Set)
import qualified Data.Set as Set
data Envi = Envi {content_envi :: Map.Map Coord [Entity]} deriving Show

instance Monoid Envi where
    mempty = Envi $ Map.empty

instance Semigroup Envi where
    (<>) a b = a




--extract ids from entities
extract_ids_entities :: [Entity] -> [Int]
extract_ids_entities ent    =   LS.map (\a -> extract_id_entity a) ent

--extract id from entity
extract_id_entity :: Entity -> Int
extract_id_entity Cow{iden=id}          = id
extract_id_entity Troll{iden=id}        = id
extract_id_entity Gobelin{iden=id}      = id 
extract_id_entity Player{iden=id}       = id
extract_id_entity Key{iden=id}          = id
extract_id_entity Potion{iden=id}       = id
extract_id_entity Chest{iden=id}        = id
extract_id_entity SetArmor{item=it}     = extract_id_equipment it
extract_id_entity Trap{iden=id}         = id



--Check coord of envi
--Precondition check_coord_exist 
look_env :: Coord -> Envi -> [Entity]
look_env coord Envi{content_envi=cont}  = 
    case (Map.lookup coord cont) of 
        Just l  ->  l
        Nothing -> []

check_coord_exist :: Coord -> Envi -> Bool
check_coord_exist coord Envi{content_envi=cont} = True


--Fordable 

--check if a coord is fordable (only the entities matter)
--Precondition check_coord_exist
fordable_env :: Coord -> Envi -> Bool
fordable_env coord envi = fordable_env_unit l
    where   l = (look_env coord envi)

-- check list contains player or mobs return True if crossable
fordable_env_unit :: [Entity] -> Bool
fordable_env_unit []        = True
fordable_env_unit (x:xs)    =
    case x of 
        Cow {}              -> False
        Player {}           -> False
        Chest {}            -> False
        Troll {}            -> False
        Gobelin {}          -> False
        otherwise           -> fordable_env_unit xs    

--Find ID
--Precondition find_id_pre
find_id :: Int -> Envi -> Maybe (Coord,Entity)
find_id id Envi{content_envi=cont} = Map.foldrWithKey (\k v acc -> check_id_entities id k v acc) Nothing cont

check_id_entities :: Int -> Coord -> [Entity] -> Maybe (Coord,Entity) -> Maybe (Coord,Entity)
check_id_entities id coord ent acc   = LS.foldl (\ b a -> check_id_entity id coord a b) acc ent 

check_id_entity :: Int -> Coord -> Entity -> Maybe (Coord,Entity) -> Maybe (Coord,Entity)
check_id_entity id coord ent@Cow{iden=i} _      | i == id           = Just (coord,ent)
check_id_entity id coord ent@Player{iden=i} _   | i == id           = Just (coord,ent)
check_id_entity id coord ent@Key{iden=i} _      | i == id           = Just (coord,ent)
check_id_entity id coord ent@Potion{iden=i} _   | i == id           = Just (coord,ent)
check_id_entity id coord ent@Trap{iden=i} _     | i == id           = Just (coord,ent)
check_id_entity id coord ent@Chest{iden=i} _    | i == id           = Just (coord,ent)
check_id_entity id coord ent@Troll{iden=i} _    | i == id           = Just (coord,ent)
check_id_entity id coord ent@Gobelin{iden=i} _  | i == id           = Just (coord,ent)
check_id_entity id coord ent@SetArmor{item=l} _ 
    | id == (extract_id_equipment l)                                = Just (coord,ent)
check_id_entity _ _ _ acc                                           = acc

--Precondition find_id
--Check correct id
find_id_pre :: Int -> Bool
find_id_pre id 
    | id < 0    =   False
    | otherwise =   True

--Remove Id 
--Precondition rm_env_id_pre
--Post-condition rm_env_id_post
rm_env_id :: Int -> Envi -> Envi
rm_env_id id env@Envi{content_envi=cont} =
    case (find_id id env) of
        Just (coord,v)  -> Envi $ Map.insert coord (LS.filter (\a -> a /= v) $ look_env coord env) cont--rm_empty_case $ Envi $ Map.map (\a -> LS.delete v a) cont
        Nothing         -> env


--Precondition remove
rm_env_id_pre :: Int -> Envi -> Bool
rm_env_id_pre id env = 
    case (find_id id env) of 
        Just (_,_)      ->  True 
        Nothing         ->  False

--Post-condition remove
rm_env_id_post :: Int -> Envi -> Bool
rm_env_id_post id env =
    case (find_id id env) of 
        Just (_)        ->  False
        Nothing         ->  True

--Function removing every cases containing empty lists
--Invariant will be respected thanks to this function       
rm_empty_case :: Envi -> Envi 
rm_empty_case Envi{content_envi=content} = Envi (Map.filter (\a -> a /=[]) content)



--Add entity to env
--Precondition add_env_pre add_env_pre_correct_coord
--Post-condition add_env_post
add_env :: Coord -> Entity -> Envi -> Envi
add_env coord v Envi{content_envi=content} = 
    Envi (Map.insertWith (\a b -> a ++ b) coord [v] content)

--Precondition add environment
--Check if the entity we want to add is compatible with the entities already existing at the coord
add_env_pre :: Coord -> Entity -> Envi -> Bool 
add_env_pre coord Cow{} env             =   fordable_env coord env
add_env_pre coord Player{} env          =   fordable_env coord env
add_env_pre coord Chest{} env           =   fordable_env coord env
add_env_pre coord Troll{} env           =   fordable_env coord env
add_env_pre coord Gobelin{} env         =   fordable_env coord env
add_env_pre _ _ _                           =   True

--Precondition add Environment 
--Check if the coord are contained inside the grid need to have access to Map_game
add_env_pre_correct_coord :: Map_game -> Coord -> Bool
add_env_pre_correct_coord Map_game{map_gw=width, map_gh=height} (C x y)
    | x>=width || x < 0 || y >= width || y < 0  = False
    | otherwise                                 = True


--Post condition add_env
--Check if the entity has been added
add_env_post :: Coord -> Entity -> Envi -> Bool
add_env_post coord ent env =
    case (find_id id env) of
        Just (_,_)      ->  True
        Nothing         ->  False
    where   id = extract_id_entity ent

--move_id move the entity you want to move and trigger the trap when it needs it
--Precondition move_id_pre move_id_pre_correct_coord
--Post-condition move_id_post
move_id :: Int -> Coord -> Envi -> Envi
move_id id coord env@Envi{content_envi=content} = 
    case (find_id id env) of
        Just (key,v)    -> rm_empty_case $ trapTrigger2 coord $ Envi (Map.insertWith (\old new -> old <> new) coord [v] l) --trapTrigger (Envi (Map.insertWith (\old new -> old <> new) coord [v] l) ) v key
        Nothing         ->  env                              -- The id does not exist
    where   l = case (rm_env_id id env) of (Envi a) -> a
            
            

isPlayer :: Entity -> Bool
isPlayer Player{} = True
isPlayer _        = False
  

--Precondition move_id, move_id_pre_correct_coord
--Check if the entity with id given can be moved to given place
move_id_pre :: Int -> Coord -> Envi -> Bool
move_id_pre id coord env =
    case (find_id id env) of
        Just (_,ent)    ->  case ent of 
            Cow{}           ->  fordable_env coord env
            Player{}        ->  fordable_env coord env
            Chest{}         ->  fordable_env coord env
            Gobelin{}       ->  fordable_env coord env
            Troll{}         ->  fordable_env coord env
            _               ->  True
        Nothing         ->  True

--Precondition move Environment
--Check if the coord we want to move the entity is contained inside the grid of the game
move_id_pre_correct_coord :: Map_game -> Coord -> Bool
move_id_pre_correct_coord Map_game{map_gw=width, map_gh=height} (C x y)
    | x>=width || x < 0 || y >= width || y < 0  = False
    | otherwise                                 = True

--Post-condition move_id
--Check if an entity with the id in argument exists
move_id_post :: Int -> Coord -> Envi -> Bool
move_id_post id coord env =
    case (LS.find (\a -> (extract_id_entity a) == id) l) of
        Just _      -> True 
        _           -> False 
    where   l = look_env coord env

--uncrementThirst_id_player id + decrease hp of player if thirst <= 0
uncrementThirst_id_player :: Int -> Int -> Envi -> Envi 
uncrementThirst_id_player id unc env = 
    case (find_id id env) of 
        Just (coord, p@Player{thirst=t,pvie=pv}) -> 
            if ((t-unc)<=0) then 
                add_env coord p{pvie=(pv-5),thirst=0} $ rm_env_id id env
            else
                add_env coord p{thirst=(t-unc)} $ rm_env_id id env
                                            
--Remove dead characters from map
rm_dead_characters :: Envi -> Envi 
rm_dead_characters Envi{content_envi=content} =
    rm_empty_case $ Envi $ Map.map (\ls_ent -> LS.filter (\a -> check_dead_character a) ls_ent) content

check_dead_character :: Entity -> Bool
check_dead_character Cow{pvie=pv}       = pv > 0
check_dead_character Troll{pvie=pv}     = pv > 0
check_dead_character Gobelin{pvie=pv}   = pv > 0
check_dead_character _                  = True

--Attack
--Precondition the id of the element we want to attack owns attack point  (a potion cannot attack for exemple)
--Post condition every dead characters disapears from the environment
--attack every enemies around 
attack_id :: Int -> Envi -> Envi 
attack_id id env@Envi{content_envi=content} = 
    rm_dead_characters $ attack_id_util ls_adj id env
    where   ls_adj          = 
                case (find_id id env ) of 
                    Just (coord,_) -> case coord of 
                        (C x y) -> [(C (x-1) y), (C (x+1) y), (C x (y-1)) ,(C x (y+1))]


attack_id_util :: [Coord] -> Int -> Envi -> Envi
attack_id_util ls id env =
    LS.foldr (\coord acc -> attack_case coord acc att) env ls 
    where   att = extract_att_from_id id env

attack_case :: Coord -> Envi -> Int -> Envi
attack_case coord env@Envi{content_envi=content} att = 
    Envi $ Map.insert coord ls_updated content
    where   ls_ent = (look_env coord env)
            ls_updated = LS.map (\ent -> attack_entity ent att) ls_ent


--alternative if damages below 0 result 0 

attack_entity :: Entity -> Int -> Entity
attack_entity ent att = 
    case (ent) of 
        Cow{pvie=pv, armor=arm}     -> ent{pvie=(pv - (getDamages att arm))}
        Player{pvie=pv, armor=arm}  -> ent{pvie=(pv - (getDamages att arm))}
        Troll{pvie=pv, armor=arm}   -> ent{pvie=(pv - (getDamages att arm))}
        Gobelin{pvie=pv, armor=arm} -> ent{pvie=(pv - (getDamages att arm))}
        otherwise                   -> ent

--Compute actual damage regarding armor given
getDamages:: Int -> Int -> Int 
getDamages att armor = att - (round $ (((* 0.2) . fromIntegral) armor))

extract_att_from_id :: Int -> Envi -> Int 
extract_att_from_id id env = 
    case (find_id id env) of 
        Just (_,ent) -> case ent of           
            Player{att=a}   ->  a
            Troll{att=a}    ->  a
            Gobelin{att=a}  ->  a
            Trap{att=a}     ->  a
            otherwise       ->  ((-1) :: Int)

--Precondition attack_id 
-- Check if the entity can attack
attack_id_pre :: Int -> Envi -> Bool
attack_id_pre id env = (extract_att_from_id id env) >= 0 


--Post-condition attack_id 
-- condition every dead characters disapears from the environment
attack_id_post :: Envi -> Bool
attack_id_post env = no_dead_characters_inv env





--- find a player
--get first player in the environment (Keeping in mind the fact we could add multi player)
get_first_player_from_envi :: Envi -> (Coord,Entity)
get_first_player_from_envi Envi{content_envi=map_envi} = 
    case (Map.elemAt 0 $ Map.filter (\ent -> ent/=[]) $ Map.mapWithKey (\key ls_entity -> LS.filter (\e -> isPlayer e) ls_entity) map_envi) of 
                    (c,ls_entity) -> (c,ls_entity !! 0)




--Interaction 

-- Second version of interactEnvi 
{- 
    For all elements pickable from the case of the player pick those.
    + For all adjacents cases loot chests
-}
-- precondition entity_player_id_pre first argument
-- post-condition interactEnvi2_post_chest
-- Interact with the environment as explained above
interactEnvi2 :: Int -> Envi -> Envi 
interactEnvi2 id_player env = 
    rm_empty_case $ lootChests coord_player player $ pick_items coord_player ls_entities player env 
    where   (coord_player,player) = 
                case (find_id id_player env) of
                    Just (c,p) -> (c,p)
            ls_entities = look_env coord_player env



--First argument is the coord of player
-- precondition second argument is a player
-- post-condition the environment does not contains anymore pickable item in coord
lootChests :: Coord -> Entity -> Envi -> Envi 
lootChests coord_player@(C x y) player env = 
    case (envi_final) of 
        Envi a -> Envi $ Map.insert coord_player ls_player_case_final  a
    where   ls_adjacent_cases = [(C (x-1) y), (C (x+1) y), (C x (y-1)), (C x (y+1))]
            ls_adj_entities = LS.foldr (\c acc -> [(c,  look_env c env )] ++ acc) [] ls_adjacent_cases :: [(Coord,[Entity])]
            (player_final, envi_final) = LS.foldr (\(c,ls_entity) (p,e) -> lootChestUnit c p ls_entity e) (player,env) ls_adj_entities :: (Entity,Envi)
            ls_player_case_final = LS.insert player_final $ LS.delete player $ look_env coord_player envi_final 


{-      
    One chest at a coord take its content and add to inventory player remove 
 -}
lootChestUnit :: Coord -> Entity -> [Entity] -> Envi -> (Entity,Envi)
lootChestUnit coord player@Player{iden=id_player, inv=inventory} ls_entity env@Envi{content_envi=content} = 
    (new_player,Envi $ (Map.insert coord new_ls_entity content))
    where   ls_chests = LS.filter (\a -> isChest a) ls_entity 
            new_player = LS.foldr (\chest_ acc -> lootChest acc chest_) player ls_chests :: Entity
            new_ls_entity = LS.filter (\a -> not $ isChest a) ls_entity

-- First argument the coord of the player 
-- Second argument list of entities in the coord 
-- Third argument The player 
-- The envi 
-- return the environment with a player updated according to all entities he picked. 
pick_items :: Coord -> [Entity] -> Entity -> Envi -> Envi 
pick_items coord ls_entity player@Player{inv=inventory, iden=id_player} Envi{content_envi=content} = rm_env_id id_player (Envi $ Map.insert coord ls_entities_after_loot content) 
    where   ls_pickable = filterPickableEntities ls_entity --pickable entities
            new_inventory  = LS.foldr (\a acc -> addToInventory acc a) inventory ls_pickable -- new inventory of the player
            ls_entities_after_loot =  LS.insert player{inv=new_inventory} $ LS.filter (\a -> not $ pickableEntity a) ls_entity 


                          
--post_condition interact no chest in adjacent cases of player after interacting (once a chest is looted it disapears) + check no pickable item in coord of player

interactEnvi2_post_chest :: Int -> Envi -> Bool 
interactEnvi2_post_chest id_player env@Envi{content_envi=content} = 
    case (LS.find (\a -> isChest a) ls_entities) of
        Nothing -> 
            case (LS.find (\a -> pickableEntity a) ls_entities_player_coord) of
                Nothing -> True 
                _       -> False
        _       -> False
    where   (coord_player@(C x y),player_entity) = 
                case (find_id id_player env) of 
                    Just (c,e) -> (c,e)
            ls_entities = LS.foldr (\a acc -> (look_env a env) ++ acc ) [] [(C (x-1) y), (C (x+1) y), (C x (y-1)), (C x (y+1))] :: [Entity]
            ls_entities_player_coord = look_env coord_player env

entity_player_pre :: Entity -> Bool 
entity_player_pre Player{} = True
entity_player_pre _        = False

--precondition argument is the id of an existing player entity
entity_player_id_pre :: Entity -> Bool 
entity_player_id_pre Player{}   = True
entity_player_id_pre _          = False


--give all adjacent cases from id
adjacentCases :: Envi -> Int -> [Coord]
adjacentCases env id = 
    case (find_id id env) of 
        Just (coord, _) -> case coord of 
            (C x y)         -> [(C (x-1) y), (C (x+1) y), (C x (y-1)),(C x (y+1) )] 



--filter pickable entities
filterPickableEntities :: [Entity] -> [Entity]
filterPickableEntities ls = LS.filter (\a -> pickableEntity a) ls




--Precondition first argument entity_player_pre
--Second argument respects lootChest_pre
lootChest :: Entity -> Entity -> Entity 
lootChest player@Player{inv=inventory} Chest{content=cont} = player{inv=newInventory}
    where   newInventory = LS.foldr (\a acc ->addToInventory acc a) inventory cont

lootChest_pre :: Entity -> Bool
lootChest_pre Chest{}   = True
lootChest_pre _         = False




--Inventory


--precondition second argument player_entity_pre 
--return environment with the player having used the item he pointed in his inventory and the grid in the case player used a key
useEnv :: Envi -> Entity -> Map_game -> (Envi, Map_game)
useEnv env player@Player{iden=id, inv=inventory} mp = 
    if (isCurrentKey inventory) then
        case (containsClosedDoors mp coord) of -- check adjacent coord contains closed doors
            Just c  -> (useKey env player coord, (openDoor mp c)) -- if it is true open the door and remove the key from the inventory of the player
            Nothing -> (env,mp)
    else 
        ((add_env coord (useEnvUtil ent) $ rm_env_id id env), mp)
    where   (coord,ent) = case (find_id id env) of Just (a,b) -> (a,b)  
            
-- First argument is a Player with Inventory navigate up            
rollInventory :: Entity -> NavigateInventory -> Entity 
rollInventory player@Player{inv=inventory} nv = player{inv=(navigateInventory inventory nv)}

useKey :: Envi -> Entity -> Coord -> Envi 
useKey env player@Player{iden=id, inv=inventory} coord = 
    rm_empty_case $ add_env coord player{inv=(removeFromInventory inventory key)} (rm_env_id id env)
    where   key = case (getCurrentItem inventory) of Just k -> k


-- precondition argument entity_player_pre
-- use the current item selected in the inventory
useEnvUtil :: Entity -> Entity
useEnvUtil player@Player{inv=inventory} = 
    case (getCurrentItem inventory) of 
        Nothing         -> player
        (Just a)        -> case a of                
            (SetArmor b)    ->  switchEquip player b
            Potion{}        ->  drinkPotion player a


-- change element already equipped for the new one
switchEquip :: Entity -> Equipment -> Entity 
switchEquip pl@Player{stuff=st} eq = 
    case st of 
        Stuff{headS=he,chest_place=ch,feet=f,hands=ha,weapon=w,legs=l} -> case eq of 
            Helmet{}    -> case he of 
                NotEquipped -> equip pl eq
                (Arm arm)   -> equip (unequip pl arm) eq
            Boots{}     -> case f of 
                NotEquipped -> equip pl eq
                (Arm arm)   -> equip (unequip pl arm) eq
            Gloves{}    -> case ha of 
                NotEquipped -> equip pl eq
                (Arm arm)   -> equip (unequip pl arm) eq
            Sword{}     -> case w of
                NotEquipped -> equip pl eq
                (Arm arm)   -> equip (unequip pl arm) eq
            Legs{}      -> case l of 
                NotEquipped -> equip pl eq
                (Arm arm)   -> equip (unequip pl arm) eq
            Chest_armor{} -> case ch of 
                NotEquipped -> equip pl eq
                (Arm arm)   -> equip (unequip pl arm) eq

--update caracteristics of Player 
unequip :: Entity -> Equipment -> Entity 
unequip pl@Player{pvMax=pvmax,att=attack,armor=arm,speed=sp,stealth=st,inv=inventory,stuff=stu} eq =
    pl{pvMax=newpvMax,att=newAtt,armor=newArmor,speed=newSpeed,stealth=newStealth,inv=newInv,stuff=newStuff}
    where   oldVit      =   extract_vita_equipment eq
            oldAtt      =   extract_attack_equipment eq
            oldStealth  =   extract_stealth_equipment eq
            oldSpeed    =   extract_speed_equipment eq
            oldArmor    =   extract_armor_equipment eq
            newInv      =   addToInventory inventory (SetArmor eq)
            newpvMax    =   pvmax-oldVit
            newAtt      =   attack - oldAtt
            newArmor    =   arm - oldArmor
            newStealth  =   st - oldStealth
            newSpeed    =   sp - oldSpeed
            newStuff    =   case (eq) of 
                Helmet{}    -> stu{headS=NotEquipped}
                Boots{}     -> stu{feet=NotEquipped}
                Gloves{}    -> stu{hands=NotEquipped}
                Sword{}     -> stu{weapon=NotEquipped}
                Legs{}      -> stu{legs=NotEquipped}
                Chest_armor{} -> stu{chest_place=NotEquipped}

--Equip armor and update statistics
equip :: Entity -> Equipment -> Entity 
equip pl@Player{pvMax=pvmax,att=attack,armor=arm,speed=sp,stealth=st,inv=inventory,stuff=stu} eq  =
    pl{pvMax=newpvMax,att=newAtt,armor=newArmor,speed=newSpeed,stealth=newStealth,inv=newInv,stuff=newStuff}
    where   equipVit      =   extract_vita_equipment eq
            equipAtt      =   extract_attack_equipment eq
            equipStealth  =   extract_stealth_equipment eq
            equipSpeed    =   extract_speed_equipment eq
            equipArmor    =   extract_armor_equipment eq
            newInv      =   removeFromInventory inventory (SetArmor eq)
            newpvMax    =   pvmax + equipVit
            newAtt      =   attack + equipAtt
            newArmor    =   arm + equipArmor
            newStealth  =   st + equipStealth
            newSpeed    =   sp + equipSpeed
            newStuff    =   case (eq) of 
                Helmet{}    -> stu{headS=(Arm eq)}
                Boots{}     -> stu{feet=(Arm eq)}
                Gloves{}    -> stu{hands=(Arm eq)}
                Sword{}     -> stu{weapon=(Arm eq)}
                Legs{}      -> stu{legs=(Arm eq)}
                Chest_armor{} -> stu{chest_place=(Arm eq)}

--First argument player
--Second argument is the Equipment which must be equiped
switchEquip_post :: Entity -> Equipment -> Bool
switchEquip_post Player{stuff=st} eq = 
    case (eq) of 
        Helmet{}    -> case (headS st) of 
            (Arm e) -> e==eq 
        Boots{}     -> case (feet st) of 
            (Arm e) -> e==eq 
        Gloves{}    -> case (hands st) of 
            (Arm e) -> e==eq 
        Sword{}     -> case (weapon st) of 
            (Arm e) -> e==eq 
        Legs{}      -> case (legs st) of 
            (Arm e) -> e==eq 
        Chest_armor{} -> case (chest_place st) of
            (Arm e) -> e==eq
       
--Utilities use
--Precondition entity_player_pre
--post condition pv lower than pvmax
healPlayer :: Entity -> Int -> Entity
healPlayer pl@Player{pvie=pv, pvMax=pvmax} healpts
    | (pv+healpts) <= pvmax     =   pl{pvie=(pv+healpts)}
    | otherwise                 =   pl{pvie=pvmax}


healPlayer_post :: Entity -> Bool
healPlayer_post Player{pvie=pv, pvMax=pvmax} = pv <= pvmax

--precondition first argument entity_player_pre
--post-condition thirst max greater or equal to thirst
drinkWater :: Entity -> Int -> Entity
drinkWater pl@Player{thirst_max=thirstmax, thirst=th} t 
    | (t + th) <= thirstmax     =  pl{thirst=(t+th)}
    | otherwise                 =  pl{thirst=thirstmax}

--check the thirst bar does not exceed thirst max
drinkWater_post :: Entity -> Bool
drinkWater_post Player{thirst_max=thirstmax, thirst=th} = th <= thirstmax

-- precondition first argument entity_player_pre 
-- precondition second argument drinkPotion_pre
-- postcondition drinkPotion_post
drinkPotion :: Entity -> Entity -> Entity 
drinkPotion pl@Player{pvie=pv, att=attack, stealth=st, speed=sp, thirst=th,inv=inventory} potion =
    case potion of 
        (Potion id Health)          -> let player = healPlayer pl 500 in player{inv=removeFromInventory inventory potion}
        (Potion id Poison)          -> pl{pvie=(pv-100), inv=(removeFromInventory inventory potion)}
        (Potion id Strength)        -> pl{att=(attack+10),inv=(removeFromInventory inventory potion)}
        (Potion id Invisibility)    -> pl{stealth=(st+10),inv=(removeFromInventory inventory potion)}
        (Potion id Speed)           -> pl{speed=(sp+10),inv=(removeFromInventory inventory potion)}
        (Potion id Water)           -> let player = drinkWater pl 100 in player{inv=removeFromInventory inventory potion}
        _                           -> pl

--Check if the argument is a potion
drinkPotion_pre :: Entity -> Bool
drinkPotion_pre Potion{}    = True
drinkPotion_pre _           = False

--check if the potion has really been remove from inventory of player
drinkPotion_post :: Entity -> Entity -> Bool
drinkPotion_post potion Player{inv=inventory} = not $ checkEntityInInventory inventory potion


--Trap manipulation 

--precondition second argument player_entity_pre
--postcondition no trap in the coord 
--check if there is a trap at the coord, trigger it, and hurts the monster or player
--check if the character is dead and remove empty cases to verify invariants
trapTrigger2 :: Coord -> Envi -> Envi 
trapTrigger2 coord env@Envi{content_envi=content} =
    case (LS.find (\a -> isTrap a) ls) of
        Nothing -> env
        Just Trap{iden=id_trap,att=attack} -> rm_dead_characters $ attack_case coord (Envi $ Map.insert coord ls_end content) attack--rm_env_id id_trap $ attack_case coord env attack
    where   ls = look_env coord env
            ls_end = LS.filter (\a -> not $ isTrap a) ls

trapTrigger_post :: Envi -> Coord -> Bool
trapTrigger_post env coord =
    case (getTrap env coord) of
        (Just a)    -> True
        Nothing     -> False


getTrap :: Envi -> Coord -> Maybe Entity
getTrap env coord 
    | (LS.length $ LS.filter (\a -> isTrap a) ls_entity) == 0   = Nothing
    | otherwise                                                 = Just ((LS.filter (\a -> isTrap a) ls_entity ) !! 0)
    where   ls_entity = look_env coord env



-- Invariant 

--One chest only 


--One chest the player can reach invariant + contains exactly one key 
correctChestOnPath_inv :: Map_game -> Envi -> Bool
correctChestOnPath_inv mp env 
    | w == (C (-1) (-1))        = False
    | hasKey player             = True 
    | otherwise                 = entitiesContainsOneChestWithOneKey ls_union_entities 
    where   w = case (find_entry_from_map_game mp) of 
                    (Just a) -> a
                    Nothing -> (C (-1) (-1)) --Coord does not exist
            ls_union_entities = unionAllEntitiesReachable mp env w [] 
            player = case (get_first_player_from_envi env) of (_,p) -> p


--Do the union of all the entites reachable from the entry and return it
unionAllEntitiesReachable :: Map_game -> Envi -> Coord -> [Coord] -> [Entity]
unionAllEntitiesReachable my_map@Map_game{map_gw=width, map_gh=height, map_content=mapg} env@Envi{content_envi=content} (C x y) ls
    | x > (width-1) || x < 0 || y < 0 || y > (height-1)                                                         = []
    | (w x y) == Wall    || (w x y) == Exit   || (w x y) == (Door EW Closed)  || (w x y) == (Door NS Closed)    = []
    | otherwise                                                         = LS.union ls_entities $ LS.union (fm (x-1) y) $ LS.union (fm (x+1) y) $ (LS.union (fm (x) (y-1)) (fm (x) (y+1)))
    where   w a b = case (Map.lookup (C a b) mapg) of
                        Just u -> u
                        Nothing -> Empty_case
            fm a b 
                | (LS.elem (C a b) ls)==True    =   []
                | otherwise                     =   unionAllEntitiesReachable my_map env (C a b) (ls++[(C a b)])
            ls_entities = look_env (C x y) env    


--check in the list of entities only one chest with one key inside it
entitiesContainsOneChestWithOneKey :: [Entity] -> Bool
entitiesContainsOneChestWithOneKey ls 
    | LS.length (LS.filter (\a -> isChest a) ls) /= 1   = False
    | otherwise                                         = case (LS.find (\a -> isChest a) ls) of 
                                                            Just a -> case (a) of 
                                                                Chest{content=ls_content} -> (LS.length (LS.filter (\b -> isKey b) ls_content)) == 1


isChest :: Entity -> Bool
isChest Chest{} = True
isChest _       = False

isKey :: Entity -> Bool
isKey Key{}     = True
isKey _         = False

--Check only one trap max on a coord
uniqueTrap_inv :: Envi -> Bool 
uniqueTrap_inv Envi{content_envi=content}   = 
    Map.foldr (\a acc -> acc && (LS.length $ (LS.filter (\u -> isTrap u) a) ) < 2) True content

isTrap :: Entity -> Bool
isTrap Trap{} = True
isTrap _      = False
--cases_inv
--Check no case with a player a mob and chest at the same time 
cases_inv :: Envi -> Bool
cases_inv Envi{content_envi=content} =
   Map.foldr (\a acc -> ((LS.length a)<=1) && acc) True (Map.map (\v -> LS.filter (\a -> cases_inv_util a) v) content)

cases_inv_util :: Entity -> Bool
cases_inv_util Cow{}        = True
cases_inv_util Troll{}      = True
cases_inv_util Gobelin{}    = True
cases_inv_util Player{}     = True
cases_inv_util Chest{}      = True
cases_inv_util (otherwise)  = False

--no dead_characters_inv
--Check no dead characters in the environment 
no_dead_characters_inv :: Envi -> Bool
no_dead_characters_inv Envi{content_envi=content} =
    not $ Map.foldr (\ls_ent acc -> not $ LS.null $ LS.filter (\a -> (not (check_dead_character a) ) || acc ) ls_ent) False content

    
check_all_invariant_envi :: Envi -> Either Error Bool
check_all_invariant_envi env
    | (different_id_inv env) == False       = Left (Error "Error invariant environment: Multiple items with the same id")
    | (no_empty_case_env env) == False      = Left (Error "Error invariant environment: Some cases contains empty lists")
    | (cases_inv env) == False              = Left (Error "Error invariant environment: Some cases contains too many entities")
    | (no_dead_characters_inv env) == False = Left (Error "Error invariant environment: Some characters have less that 0 hp and are still a part of the environment")
    | (uniqueTrap_inv env) == False         = Left (Error "Error invariant environment: Some coord have multiple traps")
    | otherwise                             = Right True


-- Check there is no case with an empty list in the environment
no_empty_case_env :: Envi -> Bool
no_empty_case_env Envi{content_envi=content} 
    |(Map.filter (\a -> a==[]) content) == Map.empty    = True
    | otherwise                                         = False

--check all different id
different_id_inv :: Envi -> Bool
different_id_inv Envi{content_envi=cont} =
    (length (LS.nub l)) == (length l)
    where   l = Map.foldr (\v acc -> acc ++ (extract_ids_entities v) ) r cont
            r = [] :: [Int]