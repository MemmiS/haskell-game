module Entity where
import Equipment
import qualified Data.Map as Map
import qualified Data.List as LS
data Entity = Cow       {iden :: Int, pvie :: Int, speed :: Int, armor :: Int, stealth :: Int, coolDown :: Int} -- attack 
            | Troll     {iden :: Int, pvie :: Int, att :: Int, armor :: Int, speed :: Int, stealth :: Int, coolDown :: Int} -- Different types of ennemies
            | Gobelin   {iden :: Int, pvie :: Int, att :: Int, armor :: Int, speed :: Int, stealth :: Int, coolDown :: Int}
            | Player    {iden :: Int, pvMax :: Int, pvie :: Int, att :: Int, armor :: Int, speed :: Int, stealth :: Int, inv :: Inventory, stuff :: StuffEquipped, thirst_max :: Int, thirst :: Int, coolDown :: Int} -- attack + inventory
            | Key       {iden :: Int} -- a key got an id plus the id of its owner  
            | Potion    {iden :: Int, buff::PotionEffect}
            | Chest     {iden :: Int, content:: [Entity]}
            | SetArmor  {item :: Equipment} -- Loot     
            | Trap      {iden :: Int, att :: Int }
            deriving (Eq,Show,Ord)

data PotionEffect = Health
    | Water 
    | Poison
    | Strength
    | Invisibility
    | Speed
    deriving (Eq,Show,Ord)

data Inventory = Inv {
                        index_cur_inventory :: Int,
                        content_inventory :: [Entity]
                    } deriving (Show,Ord)

instance Eq Inventory where
    (==) (Inv a b) (Inv c d) = (a==c) && (b==d)

instance Semigroup Inventory where
    (<>) (Inv a b) (Inv c d) = Inv c (b<>d)

instance Monoid Inventory where
    mempty = Inv (-1) []

--Allow to navigate inside the inventory
data NavigateInventory = RightN | LeftN deriving (Eq,Show)

-- Wheel of inventory 
-- precondition inventory not empty 
-- postcondition inventory not modified
navigateInventory :: Inventory -> NavigateInventory -> Inventory 
navigateInventory inventory@Inv{index_cur_inventory=ind,content_inventory=cont} RightN
    | (LS.length cont) > (ind + 1)      = inventory{index_cur_inventory=(ind+1)}
    | otherwise                             = inventory
navigateInventory inventory@Inv{index_cur_inventory=ind,content_inventory=cont} LeftN
    | (ind-1) >= 0                          = inventory{index_cur_inventory=(ind-1)}
    | otherwise                             = inventory


navigateInventory_pre :: Inventory -> Bool
navigateInventory_pre Inv{index_cur_inventory=i} 
    | i == (-1) = False
    | otherwise = True

navigateInventory_post :: Inventory -> Inventory -> Bool
navigateInventory_post Inv{content_inventory=cont1} Inv{content_inventory=cont2} 
    | LS.length cont1 > 1   = cont1 == cont2
    | otherwise             = True

--Precondition Entity is not a mob or a chest
--postcondition entity must have been added
addToInventory :: Inventory -> Entity -> Inventory 
addToInventory inventory@Inv{content_inventory=cont} ent =
    case (ent) of 
        Key{}       -> inventory{index_cur_inventory=0,content_inventory=(ent:cont)}
        Potion{}    -> inventory{index_cur_inventory=0,content_inventory=(ent:cont)}
        SetArmor{}  -> inventory{index_cur_inventory=0,content_inventory=(ent:cont)}
        otherwise   -> inventory -- not supposed to happen
--check entity is a pickableItem
addToInventory_pre :: Entity -> Bool
addToInventory_pre ent = pickableEntity ent

--addToInventory_post 
addToInventory_post :: Inventory -> Entity -> Bool 
addToInventory_post inv ent = checkEntityInInventory inv ent


--Precondition entity given already present in the inventory
--Post-condition entity removed from the inventory
removeFromInventory :: Inventory -> Entity -> Inventory
removeFromInventory inventory@Inv{content_inventory=cont} ent 
    | (LS.length cont) == 1                             = inventory{content_inventory=(LS.delete ent cont), index_cur_inventory=(-1)}
    | otherwise                                         = inventory{index_cur_inventory=0,content_inventory =(LS.delete ent cont)}

removeFromInventory_pre :: Inventory -> Entity -> Bool
removeFromInventory_pre inv ent = checkEntityInInventory inv ent

removeFromInventory_post :: Inventory -> Entity -> Bool
removeFromInventory_post inv ent = not $ checkEntityInInventory inv ent

-- getCurrentItem Item
--Precondition correct Indexes
getCurrentItem :: Inventory -> Maybe Entity 
getCurrentItem Inv{index_cur_inventory=ind,content_inventory=content} 
    | ind == (-1) = Nothing
    | otherwise   = Just (content !! ind)

getCurrentItem_pre :: Inventory -> Bool
getCurrentItem_pre Inv{index_cur_inventory=ind,content_inventory=content}
    | (LS.length content) < (ind) && (ind >= 0)     = False
    | otherwise                                     = True


checkEntityInInventory :: Inventory -> Entity -> Bool 
checkEntityInInventory Inv{content_inventory=content} ent =
    LS.elem ent content


--Current pointer on key
isCurrentKey :: Inventory -> Bool
isCurrentKey inventory = 
    case (getCurrentItem inventory) of
        Just Key{} -> True
        _          -> False

--Return True if the player got a key 
hasKey :: Entity -> Bool
hasKey Player{inv=inventory} = LS.length (LS.filter (\a -> case a of 
    Key{} -> True
    _     -> False) (content_inventory inventory)) > 0
--Take a player and return true if current item in inventory is a key
isCurrentKeyPlayer :: Entity -> Bool
isCurrentKeyPlayer Player{inv=inventory} = isCurrentKey inventory

--Invariant correct indexes
correct_index_inventory :: Inventory -> Bool
correct_index_inventory Inv{index_cur_inventory=ind , content_inventory=inventory}
    | (LS.length inventory) > ind && ind >= 0   = True
    | otherwise                                 = False



--return true if the item is pickable
pickableEntity :: Entity -> Bool
pickableEntity Potion{}     = True
pickableEntity Key{}        = True
pickableEntity SetArmor{}   = True
pickableEntity _            = False