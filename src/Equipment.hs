{-# LANGUAGE DuplicateRecordFields #-}

module Equipment where

data Equipment  = Helmet    {id :: Int, armorPts :: Int, attackPts :: Int, stealthPts :: Int, vitaPts :: Int} 
                | Chest_armor     {id :: Int, armorPts :: Int, speedPts :: Int , stealthPts :: Int, vitaPts :: Int}
                | Boots     {id :: Int, armorPts :: Int, speedPts :: Int, stealthPts :: Int, vitaPts :: Int}
                | Gloves    {id :: Int, armorPts :: Int, attackPts :: Int, vitaPts :: Int} 
                | Sword     {id :: Int, attackPts :: Int} 
                | Legs      {id :: Int, armorPts :: Int, stealthPts :: Int, vitaPts :: Int} 
                deriving (Eq,Show,Ord)

data LocationEquipped = NotEquipped | Arm Equipment deriving (Eq,Show,Ord)

--Important to check stuff compatible with the part of the body
--A player is a full set 
data StuffEquipped  = Stuff {headS :: LocationEquipped, chest_place :: LocationEquipped, feet :: LocationEquipped, hands :: LocationEquipped, weapon :: LocationEquipped, legs :: LocationEquipped}
                    deriving (Eq,Show,Ord)

instance Semigroup StuffEquipped where
    (<>) s1 s2 = s1

instance Monoid StuffEquipped where
    mempty =  Stuff NotEquipped NotEquipped NotEquipped NotEquipped NotEquipped NotEquipped

extract_id_equipment :: Equipment -> Int 
extract_id_equipment Helmet{id=ide} = ide
extract_id_equipment Boots{id=ide}  = ide
extract_id_equipment Gloves{id=ide} = ide
extract_id_equipment Sword{id=ide}  = ide
extract_id_equipment Legs{id=ide}   = ide
extract_id_equipment Chest_armor{id=ide}  = ide

extract_vita_equipment :: Equipment -> Int 
extract_vita_equipment Helmet{vitaPts=v}    = v
extract_vita_equipment Boots{vitaPts=v}     = v
extract_vita_equipment Gloves{vitaPts=v}    = v
extract_vita_equipment Legs{vitaPts=v}      = v
extract_vita_equipment Chest_armor{vitaPts=v}     = v
extract_vita_equipment _                    = 0

extract_attack_equipment :: Equipment -> Int 
extract_attack_equipment Helmet{attackPts=att}  = att
extract_attack_equipment Gloves{attackPts=att}  = att
extract_attack_equipment Sword{attackPts=att}   = att
extract_attack_equipment _                      = 0

extract_stealth_equipment :: Equipment -> Int 
extract_stealth_equipment Helmet{stealthPts=st} = st
extract_stealth_equipment Boots{stealthPts=st}  = st
extract_stealth_equipment Legs{stealthPts=st}   = st
extract_stealth_equipment Chest_armor{stealthPts=st}  = st
extract_stealth_equipment _                     = 0

extract_speed_equipment :: Equipment -> Int 
extract_speed_equipment Boots{speedPts=s}   = s
extract_speed_equipment Chest_armor{speedPts=s} = s
extract_speed_equipment _                   = 0

extract_armor_equipment :: Equipment -> Int 
extract_armor_equipment eq = 
    case (eq) of 
        Helmet{armorPts=a}  -> a
        Boots{armorPts=a}   -> a
        Gloves{armorPts=a}  -> a
        Legs{armorPts=a}    -> a
        Chest_armor{armorPts=a}   -> a
        otherwise           -> 0
