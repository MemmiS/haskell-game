module Order where
import Entity 
import Environment 
import Model
import Map_game
import System.Random
import qualified Data.List as LS
import qualified Data.Map as Map
import Control.Concurrent (threadDelay)

data Order = N | S | E | W | U | IM | AT | IN deriving (Show,Eq)


behaviour_envi :: Model -> Model 
behaviour_envi mod@Cont{envi=env} =
    LS.foldr (\ent acc -> behaviourOneEntity ent acc) mod ls_entities
    where   ls_entities = Map.foldr (\a acc -> acc ++ a) [] (content_envi env)

behaviourOneEntity :: Entity -> Model -> Model 
behaviourOneEntity ent model@Cont{envi=env} =
    case ent of 
        Troll{}     -> decide (scheduled_troll model ent) model ent
        Gobelin{}   -> decide (scheduled_goblin model ent) model ent
        Cow{}       -> decide (scheduled_cow model ent) model ent
        _           -> model


decide :: [(Int, Order)] -> Model -> Entity -> Model
decide ls mod@Cont{gene=seed} ent = 
    decideUtil order mod ent
    where   totWeight   = sumWeightListOrders ls
            random      = case (randomR (0,totWeight) seed) of (a,_)   -> a :: Int
            order       = getOrderFromListWeight ls random



decideUtil :: Order -> Model -> Entity -> Model 
decideUtil ord mod@Cont{envi=env} ent 
    | cd < (floor $ (fromIntegral 100 / fromIntegral sp))       =   mod{envi=(add_env coord (ent{coolDown=(cd+1)}) $ (rm_env_id id env))} 
    | otherwise = 
        case (ord) of 
            N   -> moveUp (modifymodelcooldown mod) ent 
            S   -> moveDown (modifymodelcooldown mod) ent
            E   -> moveRight (modifymodelcooldown mod) ent
            W   -> moveLeft (modifymodelcooldown mod) ent 
            AT  -> case player of Just(coord_player,player) -> mod{envi=attack_case coord_player (envi (modifymodelcooldown mod)) att}  --attack mod ent
            IM  -> mod
            U   -> mod  -- undefined
    where   map_env = case (envi mod) of Envi a -> a
            player = LS.foldr (\a acc -> find_player a acc) Nothing ls_visibility
            (att,id,cd,sp) = 
                case ent of 
                    Troll{att=a, iden=i,coolDown=c,speed=sp} -> (a,i,c,sp)
                    Gobelin{att=a, iden=i,coolDown=c,speed=sp} -> (a,i,c,sp)
                    Player{att=a, iden=i,coolDown=c,speed=sp}  -> (a,i,c,sp)
            Just (coord,_) = find_id id (envi mod)
            ls_visibility = LS.filter (\(_,w) -> w /= []) $ Map.foldrWithKey (\k v acc -> entitiesInsideVision k coord (LS.filter (\u -> isPlayer u) v) acc 4) [] map_env
            new_entity = 
                case ent of 
                    Troll{} -> ent{coolDown=0}
                    Gobelin{} -> ent{coolDown=0}
                    Player{} -> ent{coolDown=0}
            modifymodelcooldown m = m{envi=(add_env coord new_entity $ (rm_env_id id env))}


getOrderFromListWeight :: [(Int, Order)] -> Int -> Order
getOrderFromListWeight ls rand =
    case (LS.length ls)  of
        0   -> IM
        _   -> case (ls !! (ind)) of (_,ord) -> ord
    where   ind = case (LS.foldr (\a acc -> getOrderFromListWeightUtil a acc rand) (0::Int,(-1)::Int) ls) of (_,i) -> i

getOrderFromListWeightUtil :: (Int,Order) -> (Int, Int) -> Int -> (Int, Int)
getOrderFromListWeightUtil (w,_) (sumWeight, ind) limit
    | limit > (sumWeight+w)          = (sumWeight+w,ind)
    | limit <= (sumWeight+w)         = (sumWeight+w, ind+1)



sumWeightListOrders :: [(Int,Order)] -> Int 
sumWeightListOrders ls =
    LS.foldr (\(w,_) acc -> acc+w) 0 ls


--Here are the implementation of strategies. Real use of caract stealth to know if a mob see the player and trigger a special behaviour
--Use of Manhattan distance

manhattan_distance :: Coord -> Coord -> Int 
manhattan_distance (C x y) (C x2 y2) = (abs (x2-x)) + (abs (y2-y))

scheduled_cow :: Model -> Entity -> [(Int,Order)]
scheduled_cow model@Cont{envi=env} Cow{iden=id} = 
    case (find_id id env) of
        Just (coord,_) -> strategy_cow model coord
        Nothing          -> [] --Error occurent this id does not exists

scheduled_goblin :: Model -> Entity -> [(Int,Order)]
scheduled_goblin model@Cont{envi=env} Gobelin{iden=id} = 
    case (find_id id env) of 
        Just (coord,_) -> strategy_gobelin model coord
        Nothing -> []
scheduled_troll :: Model -> Entity -> [(Int,Order)]
scheduled_troll model@Cont{envi=env} Troll{iden=id} = 
    case (find_id id env) of 
        Just (coord,_) -> strategy_troll model coord
        Nothing -> []

--The cow would prefere to run away from the player
--We have to find an heuristic to take stealth into account
strategy_cow :: Model -> Coord -> [(Int,Order)]
strategy_cow Cont{envi=env, grid=gridd} coord =
    case (res) of
        Nothing -> LS.map (\(_,order) -> (1,order)) reachable_coord --When there is no player in its sight the cow moves randomly into the cases it can reachs
        Just(coord2,player) -> case (heuristic_stealth_cow player coord coord2) of 
            False    ->  LS.map (\(_,order) -> (10,order)) reachable_coord--The cow did not see the player despite the fact he is in its sight because of his stealth
            True     ->  LS.zip ls sorted_orders--(<*>) (fmap (,) ls) sorted_orders -- more important weights for moves allowing cow to run away from player
            where   sorted_orders = LS.map (\(c,o) -> o) $ LS.sortOn (\(c,o) -> manhattan_distance coord2 c ) reachable_coord --moves in the growing order of the manhattan distance from player 
    where   reachable_coord = (util_coord env gridd coord)
            map_env = case env of 
                Envi a -> a
            ls_visibility = Map.foldrWithKey (\k v acc -> entitiesInsideVision k coord v acc 3) [] map_env  -- all elements visibles with their coord
            res = LS.foldr (\a acc -> find_player a acc) Nothing ls_visibility
            ls = LS.map (\a -> a ^ 10) [1..(LS.length reachable_coord)]


-- Definition of behaviour of gobelins
-- gobelin packed tends to get closer and attack the player otherwise tends to run away or gets closer to other goblins
strategy_gobelin :: Model -> Coord -> [(Int,Order)]
strategy_gobelin mod@Cont{envi=env, grid=gridd} coord = 
    if (LS.length ls_gobelins)<3 then  
        strategy_cow mod coord --behaves like a cow when too few gobelins around him
    else 
        case player of 
            Nothing -> gobelin_join_others reachable_coord ls_gobelins_coord coord -- if there is no player and there are enough gobelins. The gobelin will try to get closer to the nearest gobelin
            Just (coord_player,player_entity) -> case (manhattan_distance coord coord_player) of 
                1 -> [(1,AT)]
                0 -> [] -- not supposed to happen
                _ -> 
                    case (heuristic_stealth_gobelin player_entity coord coord_player) of 
                        True    -> LS.zip ls $ LS.map (\(_,o) -> o) $ LS.sortOn (\(c,o) -> manhattan_distance coord_player c) reachable_coord -- The player was detected by the goblin.try to get closer from the player
                        False   -> gobelin_join_others reachable_coord ls_gobelins_coord coord -- The goblin did not detect the player
    where   map_env = case env of 
                Envi a -> a
            ls_visibility = Map.foldrWithKey (\k v acc -> entitiesInsideVision k coord v acc 6) [] map_env
            ls_gobelins_coord = LS.filter (\r -> strat_help r) $ LS.map (\(c,ent) -> (c,LS.find (\u -> isGobelin u) ent) ) ls_visibility
            ls_gobelins = LS.filter (\a -> isGobelin a) $ LS.foldr (\(_,a) acc -> a++acc) [] ls_visibility -- gives all the gobelins (including itself) in a radius of 6 cases
            player = LS.foldr (\a acc -> find_player a acc) Nothing ls_visibility
            reachable_coord = util_coord env gridd coord -- list of all possible moves
            ls = LS.reverse $ LS.map (\a -> a ^ 10) [1..(LS.length reachable_coord)]


util_coord_test :: Envi -> Map_game -> Coord -> IO ()
util_coord_test env gridd coord = putStrLn $ show res 
    where   res = util_coord env gridd coord


strategy_troll :: Model -> Coord -> [(Int,Order)]
strategy_troll mod@Cont{envi=env, grid=gridd} coord = 
    case player of 
        Nothing -> strategy_cow mod coord --same strategy as a cow
        Just (coord_player, player_entity) -> 
            case (manhattan_distance coord coord_player) of 
                1 -> [(1,AT)]--always detects a player in adjacent case
                0 -> [] --
                _ -> 
                    case (heuristic_stealth_troll player_entity coord coord_player) of 
                        True -> LS.zip ls $ LS.map (\(_,o) -> o) $ LS.sortOn (\(c,o) -> manhattan_distance coord_player c) reachable_coord
                        False -> strategy_cow mod coord 
    where   map_env = case env of Envi a -> a
            ls_visibility = LS.filter (\(_,w) -> w /= []) $ Map.foldrWithKey (\k v acc -> entitiesInsideVision k coord (LS.filter (\u -> isPlayer u) v) acc 4) [] map_env
            player = LS.foldr (\a acc -> find_player a acc) Nothing ls_visibility
            reachable_coord = util_coord env gridd coord
            ls = LS.reverse $ LS.map (\a -> a ^ 15) [1..(LS.length reachable_coord)]

--Troll only detects player
heuristic_stealth_troll :: Entity -> Coord -> Coord -> Bool
heuristic_stealth_troll player coord coord2 = 
    case (manhattan_distance coord coord2) of 
        4 -> st <= 4
        3 -> st <= 14
        2 -> st <= 30
        _ -> True -- Not supposed to happen
    where   st = case player of Player{stealth=s} -> s
        

strat_help :: (Coord, Maybe Entity) -> Bool
strat_help (_, Nothing) = False
strat_help _            = True

isGobelin :: Entity -> Bool
isGobelin Gobelin{} = True
isGobelin _         = False

--nub a list of weighted order summing weights 
nubListWeightedOrder :: [(Int, Order)] -> [(Int,Order)] -> [(Int,Order)]
nubListWeightedOrder sofar ((w,o):xs) = 
    case (LS.find (\(w2,o2)-> o2==o) sofar) of
        Nothing -> nubListWeightedOrder xs ((w,o):sofar)
        Just (w2,o2) -> nubListWeightedOrder xs $ ((w2+w,o2):LS.delete (w2,o2) sofar)


-- first argument : list of gobelins including in sight of gobelin (includes itself)
-- second argument : coord of the gobelin we want to know its behaviour 
gobelin_join_others :: [(Coord,Order)] -> [(Coord,Maybe Entity)] -> Coord -> [(Int, Order)]
gobelin_join_others reachable_coord ls coord = ls_orders
    where   ls_sorted = LS.sortOn (\(c,e) -> manhattan_distance coord c) $ LS.filter (\(c,_)-> c/=coord) ls :: [(Coord, Maybe Entity)] -- we remove the gobelin we are studying and sort the list according to the manhattan distance
            ls_orders = nubListWeightedOrder [] $ (<*>) (fmap (,) ([(LS.length ls_orders)..1])) $ LS.map (\gob -> gobelin_join_other gob reachable_coord coord) ls_sorted -- list of all order with weight 


gobelin_join_other :: (Coord, Maybe Entity) -> [(Coord,Order)] -> Coord -> Order
gobelin_join_other (coord,ent) reachable_coord coord2 =
    case (manhattan_distance coord coord2) of 
        1 -> IM --do nothing if the nearest gobelin is in an adjacent case
        0 -> IM --Not supposed to happen
        _ -> case (LS.sortOn (\(c,o) -> manhattan_distance coord2 c) reachable_coord) of
            ((c,o):xs) -> o -- choose the order (among the possibles orders) allowing to get closer from another gobelin 

--Gobelin only detects player, Trolls 
--Gobelins have a better vision than cows
heuristic_stealth_gobelin :: Entity -> Coord -> Coord -> Bool
heuristic_stealth_gobelin ent coord coord2 = 
    case (manhattan_distance coord coord2) of 
        6 -> st <= 5 
        5 -> st <= 10
        4 -> st <= 20
        3 -> st <= 30
        2 -> st <= 50
        1 -> st <= 80
        0 -> True --not supposed to happen
    where   st = case ent of 
                Player{stealth=s} -> s
                Troll{stealth=s}  -> s

-- precondition first argument is player
-- Cow can only detect player
-- first argument - the entity that could be seen
-- second argument - coord of first arg
-- third argument - coord of second arg
-- return true if the entity at coord2 is seen by the cow at coord 
heuristic_stealth_cow :: Entity -> Coord -> Coord -> Bool
heuristic_stealth_cow Player{stealth=st} coord coord2 = 
    case (manhattan_distance coord coord2) of 
        3 -> st <= 10 -- if the player has less than 10 in stealth stats he is seen by the cow
        2 -> st <= 30
        1 -> st <= 50
        0 -> True  --not supposed to happen because cow and player cannot be in the same case

--return a safe pair (coord,player) if there is a player in list of entity contained inside the coord
find_player :: (Coord,[Entity]) -> Maybe (Coord,Entity) -> Maybe (Coord,Entity)
find_player (coord,ent) Nothing = 
    case (LS.find (\e -> isPlayer e) ent) of 
        Just u  -> Just(coord,u)
        Nothing -> Nothing
find_player _ old_v = old_v

-- fifth argument : limit distance the entity can see
--return a list of all visible entities with their coord 
entitiesInsideVision :: Coord -> Coord -> [Entity] -> [(Coord,[Entity])] -> Int -> [(Coord,[Entity])]
entitiesInsideVision coord coordCow ls acc maxSight
    | (manhattan_distance coord coordCow) <= maxSight  = acc++[(coord,ls)]
    | otherwise                                 = acc

--Return all possible order for npc 
util_coord :: Envi -> Map_game -> Coord -> [(Coord,Order)]
util_coord env mp (C x y) = possible_moves ++  [((C x y),IM)]
    where   possible_moves = LS.foldr (\a acc -> util_coord2 env mp a acc) [] [((C (x-1) y), W), ((C (x+1) y), E), ((C x (y-1)),N), ((C x (y+1)), S)]

util_coord2 :: Envi -> Map_game -> (Coord,Order) -> [(Coord,Order)] -> [(Coord,Order)]
util_coord2 env mp (coord,order) acc 
    | (fordable_env coord env) == True && (fordable_case_map_game mp False coord) == True = acc ++ [(coord,order)]
    | otherwise                                                                           = acc