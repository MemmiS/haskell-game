{-# Language OverloadedStrings #-}
module Main where

import Map_game
import Entity
import Environment
import Model
import Equipment
import qualified Order as O


import Control.Monad (unless)
import Control.Concurrent (threadDelay)
import System.IO.Unsafe
import Control.Monad.Fix


import Data.Set (Set)
import qualified Data.Set as Set
import Data.List (foldl')
import qualified Data.List as LS
import qualified Data.Map as Map
import Foreign.C.Types (CInt (..) )




import SDL
import SDL.Time (time, delay)
import SDL.Audio
import Linear (V4(..))

import TextureMap (TextureMap, TextureId (..))
import qualified TextureMap as TM

import Sprite (Sprite)
import qualified Sprite as S

import SpriteMap (SpriteMap, SpriteId (..))
import qualified SpriteMap as SM

import Keyboard (Keyboard)
import qualified Keyboard as K

import qualified Debug.Trace as T

import qualified PlaySound
import System.Directory

import Model (Model)
import qualified Model as M

loadMushroom :: Renderer -> FilePath -> TextureMap -> SpriteMap -> Int -> Int -> IO (TextureMap, SpriteMap)
loadMushroom rdr path tmap smap x_step y_step= do 
    tmap' <- TM.loadTexture rdr path (TextureId "mushroom") tmap
    let sprite = S.defaultScale (S.addImage S.createEmptySprite $ S.createImage (TextureId "mushroom") (S.mkArea 0 0 50 50)) --(CInt $ floor $ 0.5 * fromIntegral x_step) (CInt $ floor $ 0.5 * fromIntegral y_step)
    let smap' = SM.addSprite (SpriteId "mushroom") sprite smap
    return (tmap', smap')

loadBackground :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadBackground rdr path tmap smap = do 
    tmap' <- TM.loadTexture rdr path (TextureId "background") tmap 
    let sprite = S.defaultScale $ S.addImage S.createEmptySprite $ S.createImage (TextureId "background") (S.mkArea 0 0 640 480)
    let smap' = SM.addSprite (SpriteId "background") sprite smap
    return (tmap', smap')

loadHUD :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadHUD rdr path tmap smap = do 
    tmap' <- TM.loadTexture rdr path (TextureId "hud") tmap 
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "hud") (S.mkArea 0 0 100 100)) (CInt 650) (CInt 60)
    let smap' = SM.addSprite (SpriteId "hud") sprite smap
    return (tmap', smap')

loadPerso :: Renderer -> FilePath -> TextureMap -> SpriteMap -> Int -> Int -> IO (TextureMap, SpriteMap)
loadPerso rdr path tmap smap x_step y_step= do 
    tmap' <- TM.loadTexture rdr path (TextureId "player") tmap
    let sprite = S.defaultScale (S.addImage S.createEmptySprite $ S.createImage (TextureId "player") (S.mkArea 0 0 50 50)) --(CInt $ floor $ 0.5 * fromIntegral x_step) (CInt $ floor $ 0.5 * fromIntegral y_step)
    let smap' = SM.addSprite (SpriteId "player") sprite smap
    return (tmap', smap')

loadWaterPotion :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadWaterPotion rdr path tmap smap = do 
    tmap' <- TM.loadTexture rdr path (TextureId "waterPotion") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "waterPotion") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "waterPotion") sprite smap
    return (tmap', smap')

loadPoisonPotion :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadPoisonPotion rdr path tmap smap = do 
    tmap' <- TM.loadTexture rdr path (TextureId "poisonPotion") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "poisonPotion") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "poisonPotion") sprite smap
    return (tmap', smap')

loadStealthPotion :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadStealthPotion rdr path tmap smap = do 
    tmap' <- TM.loadTexture rdr path (TextureId "stealthPotion") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "stealthPotion") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "stealthPotion") sprite smap
    return (tmap', smap')

loadStrengthPotion :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadStrengthPotion rdr path tmap smap = do 
    tmap' <- TM.loadTexture rdr path (TextureId "strengthPotion") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "strengthPotion") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "strengthPotion") sprite smap
    return (tmap', smap')

loadSpeedPotion :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadSpeedPotion rdr path tmap smap = do 
    tmap' <- TM.loadTexture rdr path (TextureId "speedPotion") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "speedPotion") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "speedPotion") sprite smap
    return (tmap', smap')


loadHealthPotion :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadHealthPotion rdr path tmap smap = do 
    tmap' <- TM.loadTexture rdr path (TextureId "healthPotion") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "healthPotion") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "healthPotion") sprite smap
    return (tmap', smap')

loadGobelin :: Renderer -> FilePath -> TextureMap -> SpriteMap -> Int -> Int -> IO (TextureMap, SpriteMap)
loadGobelin rdr path tmap smap xstep ystep = do 
    tmap' <- TM.loadTexture rdr path (TextureId "gobelin") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "gobelin") (S.mkArea 0 0 100 100)) (CInt $ fromIntegral xstep) (CInt $ fromIntegral ystep)
    let smap' = SM.addSprite (SpriteId "gobelin") sprite smap
    return (tmap', smap')

loadTroll :: Renderer -> FilePath -> TextureMap -> SpriteMap -> Int -> Int -> IO (TextureMap, SpriteMap)
loadTroll rdr path tmap smap xstep ystep= do 
    tmap' <- TM.loadTexture rdr path (TextureId "troll") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "troll") (S.mkArea 0 0 100 100)) (CInt $ fromIntegral xstep) (CInt $ fromIntegral ystep)
    let smap' = SM.addSprite (SpriteId "troll") sprite smap
    return (tmap', smap')

loadChest :: Renderer -> FilePath -> TextureMap -> SpriteMap -> Int -> Int ->  IO (TextureMap, SpriteMap)
loadChest rdr path tmap smap xstep ystep = do 
    tmap' <- TM.loadTexture rdr path (TextureId "chest") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "chest") (S.mkArea 0 0 100 100)) (CInt $ fromIntegral xstep) (CInt $ fromIntegral ystep)
    let smap' = SM.addSprite (SpriteId "chest") sprite smap
    return (tmap', smap')

loadEntry :: Renderer -> FilePath -> TextureMap -> SpriteMap -> Int -> Int ->  IO (TextureMap, SpriteMap)
loadEntry rdr path tmap smap xstep ystep = do 
    tmap' <- TM.loadTexture rdr path (TextureId "entry") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "entry") (S.mkArea 0 0 100 100)) (CInt $ fromIntegral xstep) (CInt $ fromIntegral ystep)
    let smap' = SM.addSprite (SpriteId "entry") sprite smap
    return (tmap', smap')

loadOpenedDoorEW :: Renderer -> FilePath -> TextureMap -> SpriteMap -> Int -> Int -> IO (TextureMap, SpriteMap)
loadOpenedDoorEW rdr path tmap smap xstep ystep = do 
    tmap' <- TM.loadTexture rdr path (TextureId "openedDoorEW") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "openedDoorEW") (S.mkArea 0 0 100 100)) (CInt $ fromIntegral xstep) (CInt $ fromIntegral ystep)
    let smap' = SM.addSprite (SpriteId "openedDoorEW") sprite smap
    return (tmap', smap')

loadOpenedDoorNS :: Renderer -> FilePath -> TextureMap -> SpriteMap -> Int -> Int -> IO (TextureMap, SpriteMap)
loadOpenedDoorNS rdr path tmap smap xstep ystep= do 
    tmap' <- TM.loadTexture rdr path (TextureId "openedDoorNS") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "openedDoorNS") (S.mkArea 0 0 100 100)) (CInt $ fromIntegral xstep) (CInt $ fromIntegral ystep)
    let smap' = SM.addSprite (SpriteId "openedDoorNS") sprite smap
    return (tmap', smap')

loadClosedDoorNS :: Renderer -> FilePath -> TextureMap -> SpriteMap -> Int -> Int -> IO (TextureMap, SpriteMap)
loadClosedDoorNS rdr path tmap smap xstep ystep= do 
    tmap' <- TM.loadTexture rdr path (TextureId "closedDoorNS") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "closedDoorNS") (S.mkArea 0 0 100 100)) (CInt $ fromIntegral xstep) (CInt $ fromIntegral ystep)
    let smap' = SM.addSprite (SpriteId "closedDoorNS") sprite smap
    return (tmap', smap')

loadClosedDoorEW :: Renderer -> FilePath -> TextureMap -> SpriteMap -> Int -> Int -> IO (TextureMap, SpriteMap)
loadClosedDoorEW rdr path tmap smap xstep ystep= do 
    tmap' <- TM.loadTexture rdr path (TextureId "closedDoorEW") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "closedDoorEW") (S.mkArea 0 0 10 10)) (CInt $ fromIntegral xstep) (CInt $ fromIntegral ystep)
    let smap' = SM.addSprite (SpriteId "closedDoorEW") sprite smap
    return (tmap', smap') 

loadWall :: Renderer -> FilePath -> TextureMap -> SpriteMap -> Int -> Int ->  IO (TextureMap, SpriteMap)
loadWall rdr path tmap smap xstep ystep = do 
    tmap' <- TM.loadTexture rdr path (TextureId "wall") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "wall") (S.mkArea 0 0 300 300)) (CInt $ fromIntegral xstep) (CInt $ fromIntegral ystep)
    let smap' = SM.addSprite (SpriteId "wall") sprite smap
    return (tmap', smap')

loadTrap :: Renderer -> FilePath -> TextureMap -> SpriteMap -> Int -> Int ->  IO (TextureMap, SpriteMap)
loadTrap rdr path tmap smap xstep ystep = do 
    tmap' <- TM.loadTexture rdr path (TextureId "trap") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "trap") (S.mkArea 0 0 300 300)) (CInt $ fromIntegral xstep) (CInt $ fromIntegral ystep)
    let smap' = SM.addSprite (SpriteId "trap") sprite smap
    return (tmap', smap')


loadKey :: Renderer -> FilePath -> TextureMap -> SpriteMap -> Int -> Int ->  IO (TextureMap, SpriteMap)
loadKey rdr path tmap smap x_step y_step = do 
    tmap' <- TM.loadTexture rdr path (TextureId "key") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "key") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "key") sprite smap
    return (tmap', smap')

loadExit :: Renderer -> FilePath -> TextureMap -> SpriteMap -> Int -> Int ->  IO (TextureMap, SpriteMap)
loadExit rdr path tmap smap x_step y_step = do 
    tmap' <- TM.loadTexture rdr path (TextureId "exit") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "exit") (S.mkArea 0 0 300 300)) (CInt $ fromIntegral x_step) (CInt $ fromIntegral y_step)
    let smap' = SM.addSprite (SpriteId "exit") sprite smap
    return (tmap', smap')

loadPeasantHelmet :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadPeasantHelmet rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "peasant_helmet") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "peasant_helmet") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "peasant_helmet") sprite smap
    return (tmap', smap')

loadLeatherHelmet :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadLeatherHelmet rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "leather_helmet") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "leather_helmet") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "leather_helmet") sprite smap
    return (tmap', smap')

loadThiefHelmet :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadThiefHelmet rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "thief_helmet") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "thief_helmet") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "thief_helmet") sprite smap
    return (tmap', smap')

loadChainHelmet :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadChainHelmet rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "chain_helmet") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "chain_helmet") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "chain_helmet") sprite smap
    return (tmap', smap')

loadPlateHelmet :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadPlateHelmet rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "plate_helmet") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "plate_helmet") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "plate_helmet") sprite smap
    return (tmap', smap')



loadPeasantLegs :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadPeasantLegs rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "peasant_legs") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "peasant_legs") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "peasant_legs") sprite smap
    return (tmap', smap')

loadLeatherLegs :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadLeatherLegs rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "leather_legs") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "leather_legs") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "leather_legs") sprite smap
    return (tmap', smap')

loadThiefLegs :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadThiefLegs rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "thief_legs") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "thief_legs") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "thief_legs") sprite smap
    return (tmap', smap')

loadChainLegs :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadChainLegs rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "chain_legs") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "chain_legs") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "chain_legs") sprite smap
    return (tmap', smap')

loadPlateLegs :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadPlateLegs rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "plate_legs") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "plate_legs") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "plate_legs") sprite smap
    return (tmap', smap')
--

loadPeasantShoes :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadPeasantShoes rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "peasant_shoes") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "peasant_shoes") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "peasant_shoes") sprite smap
    return (tmap', smap')

loadLeatherShoes :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadLeatherShoes rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "leather_shoes") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "leather_shoes") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "leather_shoes") sprite smap
    return (tmap', smap')

loadThiefShoes :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadThiefShoes rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "thief_shoes") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "thief_shoes") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "thief_shoes") sprite smap
    return (tmap', smap')

loadChainShoes :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadChainShoes rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "chain_shoes") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "chain_shoes") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "chain_shoes") sprite smap
    return (tmap', smap')

loadPlateShoes :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadPlateShoes rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "plate_shoes") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "plate_shoes") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "plate_shoes") sprite smap
    return (tmap', smap')

--
loadPeasantGloves :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadPeasantGloves rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "peasant_gloves") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "peasant_gloves") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "peasant_gloves") sprite smap
    return (tmap', smap')

loadLeatherGloves :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadLeatherGloves rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "leather_gloves") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "leather_gloves") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "leather_gloves") sprite smap
    return (tmap', smap')

loadThiefGloves :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadThiefGloves rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "thief_gloves") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "thief_gloves") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "thief_gloves") sprite smap
    return (tmap', smap')

loadChainGloves :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadChainGloves rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "chain_gloves") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "chain_gloves") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "chain_gloves") sprite smap
    return (tmap', smap')

loadPlateGloves :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadPlateGloves rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "plate_gloves") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "plate_gloves") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "plate_gloves") sprite smap
    return (tmap', smap')
--

loadPeasantChest :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadPeasantChest rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "peasant_chest") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "peasant_chest") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "peasant_chest") sprite smap
    return (tmap', smap')

loadLeatherChest :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadLeatherChest rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "leather_chest") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "leather_chest") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "leather_chest") sprite smap
    return (tmap', smap')

loadThiefChest :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadThiefChest rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "thief_chest") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "thief_chest") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "thief_chest") sprite smap
    return (tmap', smap')

loadChainChest :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadChainChest rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "chain_chest") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "chain_chest") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "chain_chest") sprite smap
    return (tmap', smap')

loadPlateChest :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadPlateChest rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "plate_chest") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "plate_chest") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "plate_chest") sprite smap
    return (tmap', smap')

-- 

loadT1Sword :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadT1Sword rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "sword_t1") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "sword_t1") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "sword_t1") sprite smap
    return (tmap', smap')

loadT2Sword :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadT2Sword rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "sword_t2") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "sword_t2") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "sword_t2") sprite smap
    return (tmap', smap')

loadT3Sword :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadT3Sword rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "sword_t3") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "sword_t3") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "sword_t3") sprite smap
    return (tmap', smap')

loadT4Sword :: Renderer -> FilePath -> TextureMap -> SpriteMap -> IO (TextureMap, SpriteMap)
loadT4Sword rdr path tmap smap  = do 
    tmap' <- TM.loadTexture rdr path (TextureId "sword_t4") tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId "sword_t4") (S.mkArea 0 0 100 100)) (CInt 40) (CInt 40)
    let smap' = SM.addSprite (SpriteId "sword_t4") sprite smap
    return (tmap', smap')



loadBarUtil :: Renderer -> FilePath -> TextureMap -> SpriteMap -> Int ->IO (TextureMap, SpriteMap)
loadBarUtil rdr path tmap smap i = do 
    tmap' <- TM.loadTexture rdr path (TextureId str) tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId str) (S.mkArea 0 0 100 100)) (CInt 80) (CInt 20)
    let smap' = SM.addSprite (SpriteId str) sprite smap
    return $ (tmap', smap')
    where   str = "vida" <> (show i)

loadDrinkBarUtil :: Renderer -> FilePath -> TextureMap -> SpriteMap -> Int -> IO (TextureMap, SpriteMap)
loadDrinkBarUtil rdr path tmap smap i = do 
    tmap' <- TM.loadTexture rdr path (TextureId str) tmap
    let sprite = S.scale (S.addImage S.createEmptySprite $ S.createImage (TextureId str) (S.mkArea 0 0 100 100)) (CInt 120) (CInt 10)
    let smap' = SM.addSprite (SpriteId str) sprite smap
    return $ (tmap', smap')
    where   str = "drink" <> (show i)


main :: IO ()
main = do 
    initializeAll
    window <- createWindow "Game" $ defaultWindow { windowInitialSize = V2 640 540}
    renderer <- createRenderer window (-1) defaultRenderer
    let gameState = M.initModel
    let (x_step,y_step) = (floor $ fromIntegral 640 / fromIntegral (map_gw $ grid gameState),floor $ fromIntegral 480 / fromIntegral (map_gh $ grid gameState)) 
    (tmap, smap) <- loadBackground renderer "assets/background.bmp" TM.createTextureMap SM.createSpriteMap
    (tmap', smap') <- loadHUD renderer "assets/copper_hud.bmp" tmap smap
    (tmap', smap') <- loadPerso renderer "assets/player.bmp" tmap' smap' x_step y_step
    (tmap', smap') <- loadExit renderer "assets/exit.bmp" tmap' smap' x_step y_step
    (tmap', smap') <- loadWaterPotion renderer "assets/potions/waterPotion.bmp" tmap' smap'
    (tmap', smap') <- loadTroll renderer "assets/mobs/troll.bmp" tmap' smap' x_step y_step
    (tmap', smap') <- loadGobelin renderer "assets/mobs/gobelin.bmp" tmap' smap' x_step y_step
    (tmap', smap') <- loadHealthPotion renderer "assets/potions/healthPotion.bmp" tmap' smap' 
    (tmap', smap') <- loadStrengthPotion renderer "assets/potions/strengthPotion.bmp" tmap' smap'
    (tmap', smap') <- loadStealthPotion renderer "assets/potions/stealthPotion.bmp" tmap' smap'
    (tmap', smap') <- loadSpeedPotion renderer "assets/potions/speedPotion.bmp" tmap' smap' 
    (tmap', smap') <- loadPoisonPotion renderer "assets/potions/poisonPotion.bmp" tmap' smap'
    (tmap', smap') <- loadChest renderer "assets/chest.bmp" tmap' smap' x_step y_step
    (tmap', smap') <- loadWall renderer "assets/wall.bmp" tmap' smap' x_step y_step
    (tmap', smap') <- loadClosedDoorEW renderer "assets/closedDoorEW.bmp" tmap' smap' x_step y_step
    (tmap', smap') <- loadClosedDoorNS renderer "assets/closedDoorSN.bmp" tmap' smap' x_step y_step
    (tmap', smap') <- loadTrap renderer "assets/trap.bmp" tmap' smap' x_step y_step
    (tmap', smap') <- loadKey renderer "assets/key.bmp" tmap' smap' x_step y_step
    (tmap', smap') <- loadMushroom renderer "assets/mobs/mushroom.bmp" tmap' smap' x_step y_step
    (tmap', smap') <- loadEntry renderer "assets/exit.bmp" tmap' smap' x_step y_step

    (tmap', smap') <- loadT1Sword renderer "assets/armor/sword_t1.bmp" tmap' smap'
    (tmap', smap') <- loadT2Sword renderer "assets/armor/sword_t2.bmp" tmap' smap'
    (tmap', smap') <- loadT3Sword renderer "assets/armor/sword_t3.bmp" tmap' smap'
    (tmap', smap') <- loadT4Sword renderer "assets/armor/sword_t4.bmp" tmap' smap'
    (tmap', smap') <- loadPeasantChest renderer "assets/armor/peasant_chest.bmp" tmap' smap'
    (tmap', smap') <- loadPeasantGloves renderer "assets/armor/peasant_gloves.bmp" tmap' smap'
    (tmap', smap') <- loadPeasantHelmet renderer "assets/armor/peasant_helmet.bmp" tmap' smap'
    (tmap', smap') <- loadPeasantLegs renderer "assets/armor/peasant_legs.bmp" tmap' smap'
    (tmap', smap') <- loadPeasantShoes renderer "assets/armor/peasant_shoes.bmp" tmap' smap'

    (tmap', smap') <- loadThiefChest renderer "assets/armor/thief_chest.bmp" tmap' smap'
    (tmap', smap') <- loadThiefGloves renderer "assets/armor/thief_gloves.bmp" tmap' smap'
    (tmap', smap') <- loadThiefHelmet renderer "assets/armor/thief_helmet.bmp" tmap' smap'
    (tmap', smap') <- loadThiefLegs renderer "assets/armor/thief_legs.bmp" tmap' smap'
    (tmap', smap') <- loadThiefShoes renderer "assets/armor/thief_shoes.bmp" tmap' smap'

    (tmap', smap') <- loadLeatherChest renderer "assets/armor/leather_chest.bmp" tmap' smap'
    (tmap', smap') <- loadLeatherGloves renderer "assets/armor/leather_gloves.bmp" tmap' smap'
    (tmap', smap') <- loadLeatherHelmet renderer "assets/armor/leather_helmet.bmp" tmap' smap'
    (tmap', smap') <- loadLeatherLegs renderer "assets/armor/leather_legs.bmp" tmap' smap'
    (tmap', smap') <- loadLeatherShoes renderer "assets/armor/leather_shoes.bmp" tmap' smap'

    (tmap', smap') <- loadChainChest renderer "assets/armor/chain_chest.bmp" tmap' smap'
    (tmap', smap') <- loadChainGloves renderer "assets/armor/chain_gloves.bmp" tmap' smap'
    (tmap', smap') <- loadChainHelmet renderer "assets/armor/chain_helmet.bmp" tmap' smap'
    (tmap', smap') <- loadChainLegs renderer "assets/armor/chain_legs.bmp" tmap' smap'
    (tmap', smap') <- loadChainShoes renderer "assets/armor/chain_shoes.bmp" tmap' smap'

    (tmap', smap') <- loadPlateChest renderer "assets/armor/plate_chest.bmp" tmap' smap'
    (tmap', smap') <- loadPlateGloves renderer "assets/armor/plate_gloves.bmp" tmap' smap'
    (tmap', smap') <- loadPlateHelmet renderer "assets/armor/plate_helmet.bmp" tmap' smap'
    (tmap', smap') <- loadPlateLegs renderer "assets/armor/plate_legs.bmp" tmap' smap'
    (tmap', smap') <- loadPlateShoes renderer "assets/armor/plate_shoes.bmp" tmap' smap'

    (tmap', smap') <- loadBarUtil renderer "assets/hpBar/VIDA_0.bmp" tmap' smap' 0
    (tmap', smap') <- loadBarUtil renderer "assets/hpBar/VIDA_1.bmp" tmap' smap' 1
    (tmap', smap') <- loadBarUtil renderer "assets/hpBar/VIDA_2.bmp" tmap' smap' 2
    (tmap', smap') <- loadBarUtil renderer "assets/hpBar/VIDA_3.bmp" tmap' smap' 3
    (tmap', smap') <- loadBarUtil renderer "assets/hpBar/VIDA_4.bmp" tmap' smap' 4 
    (tmap', smap') <- loadBarUtil renderer "assets/hpBar/VIDA_5.bmp" tmap' smap' 5
    (tmap', smap') <- loadBarUtil renderer "assets/hpBar/VIDA_6.bmp" tmap' smap' 6
    (tmap', smap') <- loadBarUtil renderer "assets/hpBar/VIDA_7.bmp" tmap' smap' 7
    (tmap', smap') <- loadBarUtil renderer "assets/hpBar/VIDA_8.bmp" tmap' smap' 8 
    (tmap', smap') <- loadBarUtil renderer "assets/hpBar/VIDA_9.bmp" tmap' smap' 9
    (tmap', smap') <- loadBarUtil renderer "assets/hpBar/VIDA_10.bmp" tmap' smap' 10

    (tmap', smap') <- loadDrinkBarUtil renderer "assets/thirstBar/drink-0.bmp" tmap' smap' 0
    (tmap', smap') <- loadDrinkBarUtil renderer "assets/thirstBar/drink-1.bmp" tmap' smap' 1
    (tmap', smap') <- loadDrinkBarUtil renderer "assets/thirstBar/drink-2.bmp" tmap' smap' 2
    (tmap', smap') <- loadDrinkBarUtil renderer "assets/thirstBar/drink-3.bmp" tmap' smap' 3
    (tmap', smap') <- loadDrinkBarUtil renderer "assets/thirstBar/drink-4.bmp" tmap' smap' 4 
    (tmap', smap') <- loadDrinkBarUtil renderer "assets/thirstBar/drink-5.bmp" tmap' smap' 5
    (tmap', smap') <- loadDrinkBarUtil renderer "assets/thirstBar/drink-6.bmp" tmap' smap' 6
    (tmap', smap') <- loadDrinkBarUtil renderer "assets/thirstBar/drink-7.bmp" tmap' smap' 7
    (tmap', smap') <- loadDrinkBarUtil renderer "assets/thirstBar/drink-8.bmp" tmap' smap' 8 
    (tmap', smap') <- loadDrinkBarUtil renderer "assets/thirstBar/drink-9.bmp" tmap' smap' 9
    (tmap', smap') <- loadDrinkBarUtil renderer "assets/thirstBar/drink-10.bmp" tmap' smap' 10
    let kbd = K.createKeyboard 
    PlaySound.withAudio $ do 
        gameLoop 60 renderer tmap' smap' kbd gameState
    where   ls_str = LS.foldr (\a acc -> acc ++ ["vida"<>(show a)]) [] [0..10]


display_hp :: Renderer -> SpriteMap -> TextureMap -> Entity -> IO () 
display_hp rdr smap tmap Player{pvie=pv,pvMax=pvmax} 
    | ratio_hp < 0.1 = S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "vida1") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))
    | ratio_hp < 0.2 = S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "vida2") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))
    | ratio_hp < 0.3 = S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "vida3") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))
    | ratio_hp < 0.4 = S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "vida4") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))   
    | ratio_hp < 0.5 = S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "vida5") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))     
    | ratio_hp < 0.6 = S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "vida6") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))    
    | ratio_hp < 0.7 = S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "vida7") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))    
    | ratio_hp < 0.8 = S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "vida8") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))  
    | ratio_hp < 0.9 = S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "vida9") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))     
    | ratio_hp <= 1.2 = S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "vida10") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))                   
    where   ratio_hp = fromIntegral pv / fromIntegral pvmax
            (x_final,y_final) = (80,500)


display_set :: Renderer -> SpriteMap -> TextureMap -> Entity -> IO ()
display_set rdr smap tmap Player{stuff=st} = head<>foot<>hand<>wp<>chestp<>legss
    
    
    where   head    =   case (headS st) of 
                            NotEquipped ->  mempty
                            Arm eq      ->  display_equipment rdr smap tmap eq 430 500 30 30
            foot    =   case (feet st) of 
                            NotEquipped ->  mempty
                            Arm eq      ->  display_equipment rdr smap tmap eq 550 500 30 30
            hand    =   case (hands st) of 
                            NotEquipped ->  mempty
                            Arm eq      ->  display_equipment rdr smap tmap eq 490 500 30 30
            wp      =   case (weapon st) of 
                            NotEquipped -> mempty
                            Arm eq  ->  display_equipment rdr smap tmap eq 400 500 30 30
            chestp =    case (chest_place st) of 
                            NotEquipped -> mempty
                            Arm eq  -> display_equipment rdr smap tmap eq 460 500 30 30
            legss   =   case (legs st) of 
                            NotEquipped -> mempty
                            Arm eq  -> display_equipment rdr smap tmap eq 520 500 30 30


displayMap_envi :: Renderer -> SpriteMap -> TextureMap -> Envi -> Int -> Int -> IO ()
displayMap_envi rdr smap tmap Envi{content_envi = content} widthGrid heightGrid  = Map.foldrWithKey (\k ca acc -> acc <> displayOneEnviUtil rdr smap tmap k ca widthGrid heightGrid) mempty content


displayOneEnviUtil :: Renderer -> SpriteMap -> TextureMap -> Coord -> [Entity] -> Int -> Int -> IO ()
displayOneEnviUtil rdr smap tmap coord ls_entity widthGrid heightGrid = LS.foldr (\a acc -> acc <> displayOneEntity rdr smap tmap coord a (LS.length ls_entity) widthGrid heightGrid ) mempty ls_entity

displayOneEntity :: Renderer -> SpriteMap -> TextureMap -> Coord -> Entity -> Int -> Int -> Int -> IO () 
displayOneEntity rdr smap tmap (C x y) ent nb_entSameCoord widthGrid heightGrid = 
    case ent of 
        Player{} -> S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "player") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))
        Potion{buff=Water} -> S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "waterPotion") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))
        Potion{buff=Poison} -> S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "poisonPotion") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))
        Potion{buff=Health} -> S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "healthPotion") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) 
        Potion{buff=Speed} -> S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "speedPotion") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))                                                                                                     
        Potion{buff=Invisibility} -> S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "stealthPotion") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))
        Potion{buff=Strength} -> S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "strengthPotion") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))
        Gobelin{} -> S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "gobelin") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))
        Troll{}   -> S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "troll") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))
        Chest{}   -> S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "chest") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))
        Key{}     -> S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "key") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))
        Trap{}    -> S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "trap") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))
        Cow{}     -> S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "mushroom") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))
        SetArmor{item=i}      ->  display_equipment rdr smap tmap i x_final y_final 40 40
    where   (x_final,y_final) = (floor $ (fromIntegral x / fromIntegral widthGrid) * fromIntegral 640 ,floor $ (fromIntegral y / fromIntegral heightGrid) * fromIntegral 480)--(floor (((fromIntegral x / fromIntegral widthGrid) * fromIntegral 640)/ fromIntegral nb_entSameCoord), floor (((fromIntegral y / fromIntegral heightGrid) * fromIntegral 480)/ fromIntegral nb_entSameCoord))
            (stepxFinal, stepyFinal) = (floor (fromIntegral 640 / fromIntegral widthGrid), floor (fromIntegral 480 / fromIntegral heightGrid))
            (paddingx,paddingy) = (floor $ (fromIntegral stepxFinal / (fromIntegral 3)), floor $ (fromIntegral stepyFinal / (fromIntegral 3)))


display_equipment :: Renderer -> SpriteMap -> TextureMap -> Equipment -> Int -> Int -> Int -> Int -> IO ()
display_equipment rdr smap tmap Helmet{armorPts=a} x_final y_final width height
    | (a < 10)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "peasant_helmet") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | (a < 20)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "leather_helmet") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | (a < 30)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "thief_helmet") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | (a < 40)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "chain_helmet") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | otherwise     = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "plate_helmet") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
display_equipment rdr smap tmap Chest_armor{armorPts=a} x_final y_final width height
    | (a < 10)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "peasant_chest") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | (a < 20)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "leather_chest") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | (a < 30)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "thief_chest") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | (a < 40)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "chain_chest") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | otherwise     = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "plate_chest") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
display_equipment rdr smap tmap Gloves{armorPts=a} x_final y_final width height
    | (a < 10)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "peasant_gloves") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | (a < 20)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "leather_gloves") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | (a < 30)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "thief_gloves") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | (a < 40)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "chain_gloves") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | otherwise     = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "plate_gloves") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
display_equipment rdr smap tmap Boots{armorPts=a} x_final y_final width height
    | (a < 10)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "peasant_shoes") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | (a < 20)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "leather_shoes") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | (a < 30)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "thief_shoes") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | (a < 40)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "chain_shoes") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | otherwise     = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "plate_shoes") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
display_equipment rdr smap tmap Legs{armorPts=a} x_final y_final width height
    | (a < 10)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "peasant_legs") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | (a < 20)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "leather_legs") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | (a < 30)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "thief_legs") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | (a < 40)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "chain_legs") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | otherwise     = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "plate_legs") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
display_equipment rdr smap tmap Sword{attackPts=a} x_final y_final width height
    | (a < 10)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "sword_t1") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | (a < 20)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "sword_t2") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | (a < 30)      = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "sword_t3") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)
    | otherwise     = S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "sword_t4") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final))) (CInt $ fromIntegral width) (CInt $ fromIntegral height)


display_inventory :: Renderer -> SpriteMap -> TextureMap -> Entity -> IO ()
display_inventory rdr smap tmap Player{inv=inventory} =
    case (getCurrentItem inventory) of 
        Just Potion{buff=Water} -> S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "waterPotion") smap)
                                                                        (fromIntegral (320))
                                                                        (fromIntegral (500))) (CInt $ fromIntegral 20) (CInt $ fromIntegral 20)
        Just Potion{buff=Health} -> S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "healthPotion") smap)
                                                                        (fromIntegral (320))
                                                                        (fromIntegral (500))) (CInt $ fromIntegral 20) (CInt $ fromIntegral 20)
        Just Potion{buff=Invisibility} -> S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "stealthPotion") smap)
                                                                        (fromIntegral (320))
                                                                        (fromIntegral (500))) (CInt $ fromIntegral 20) (CInt $ fromIntegral 20)                                                                
        Just Potion{buff=Strength} -> S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "strengthPotion") smap)
                                                                        (fromIntegral (320))
                                                                        (fromIntegral (500))) (CInt $ fromIntegral 20) (CInt $ fromIntegral 20)
        Just Potion{buff=Speed} -> S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "speedPotion") smap)
                                                                        (fromIntegral (320))
                                                                        (fromIntegral (500))) (CInt $ fromIntegral 20) (CInt $ fromIntegral 20)
        Just Potion{buff=Poison} -> S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "poisonPotion") smap)
                                                                        (fromIntegral (320))
                                                                        (fromIntegral (500))) (CInt $ fromIntegral 20) (CInt $ fromIntegral 20)
        Just Key{} -> S.displaySprite rdr tmap $ S.scale (S.moveTo (SM.fetchSprite (SpriteId "key") smap)
                                                                        (fromIntegral (320))
                                                                        (fromIntegral (500))) (CInt $ fromIntegral 20) (CInt $ fromIntegral 20)

        Just SetArmor{item=i}      ->  display_equipment rdr smap tmap i 320 490 40 40
        Nothing -> mempty


displayMap_game :: Renderer -> SpriteMap -> TextureMap -> Map_game -> IO ()
displayMap_game rdr smap tmap Map_game{map_content=content, map_gw=width, map_gh=height} = Map.foldrWithKey (\k ca acc -> acc <> displayOneUtil rdr smap tmap k ca width height) mempty content

displayOneUtil :: Renderer -> SpriteMap -> TextureMap -> Coord -> Case -> Int -> Int -> IO ()
displayOneUtil rdr smap tmap (C x y) cas widthGrid heightGrid= 
    case cas of 
        Wall -> S.displaySprite rdr tmap (S.moveTo (SM.fetchSprite (SpriteId "wall") smap)
                                                                        (fromIntegral (x_final))
                                                                        (fromIntegral (y_final)))
        Enter -> S.displaySprite rdr tmap (S.moveTo (SM.fetchSprite (SpriteId "entry") smap)
                                                                        (fromIntegral (x_final))
                                                                        (fromIntegral (y_final)))
        Exit -> S.displaySprite rdr tmap (S.moveTo (SM.fetchSprite (SpriteId "exit") smap)
                                                                        (fromIntegral (x_final))
                                                                        (fromIntegral (y_final)))
        (Door EW Closed) -> S.displaySprite rdr tmap (S.moveTo (SM.fetchSprite (SpriteId "closedDoorEW") smap)
                                                                        (fromIntegral (x_final))
                                                                        (fromIntegral (y_final)))
        (Door NS Closed) -> S.displaySprite rdr tmap (S.moveTo (SM.fetchSprite (SpriteId "closedDoorNS") smap)
                                                                        (fromIntegral (x_final))
                                                                        (fromIntegral (y_final)))
        (Door _ Opened) -> mempty

        

        Empty_case -> mempty
    where   (x_final,y_final) = (floor ((fromIntegral x / fromIntegral widthGrid) * fromIntegral 640), floor ((fromIntegral y / fromIntegral heightGrid) * fromIntegral 480))


display_thirst :: Renderer -> SpriteMap -> TextureMap -> Entity -> IO () 
display_thirst rdr smap tmap Player{thirst=th,thirst_max=thmax} 
    | ratio_thirst < 0.1 = mempty {-S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "drink1") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))-}
    | ratio_thirst < 0.2 = S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "drink2") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))
    | ratio_thirst < 0.3 = S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "drink3") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))
    | ratio_thirst < 0.4 = S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "drink4") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))   
    | ratio_thirst < 0.5 = S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "drink5") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))     
    | ratio_thirst < 0.6 = S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "drink6") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))    
    | ratio_thirst < 0.7 = S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "drink7") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))    
    | ratio_thirst < 0.8 = S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "drink8") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))  
    | ratio_thirst < 0.9 = S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "drink9") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))     
    | ratio_thirst <= 1.2 = S.displaySprite rdr tmap $ (S.moveTo (SM.fetchSprite (SpriteId "drink10") smap)
                                                                    (fromIntegral (x_final))
                                                                    (fromIntegral (y_final)))                   
    where   ratio_thirst = fromIntegral th / fromIntegral thmax
            (x_final,y_final) = (80,525)


gameLoop :: (RealFrac a, Show a) => a -> Renderer -> TextureMap -> SpriteMap -> Keyboard -> Model -> IO ()
gameLoop frameRate renderer tmap smap kbd gameState@Cont{envi=env,logu=log,grid=gridd} = do 
    startTime <- time 
    events <- pollEvents
    let kbd' = K.handleEvents (LS.take 100 events) kbd
    clear renderer 
    --display background 
    S.displaySprite renderer tmap (SM.fetchSprite (SpriteId "background") smap)
    --display hud
    S.displaySprite renderer tmap (S.moveTo (SM.fetchSprite (SpriteId "hud") smap) (fromIntegral (0)) (fromIntegral (480)))
    --display perso
    putStrLn "begining display environment"
    displayMap_envi renderer smap tmap env (map_gw gridd) (map_gh gridd)
    putStrLn "End display environment"
    putStrLn "Begining display map_game"
    displayMap_game renderer smap tmap gridd 
    putStrLn "End display map_game"
    let (coord_player, player_entity) = get_first_player_from_envi env
    let (x_player, y_player) = case coord_player of (C x y) -> (x,y)
    --display hp
    display_hp renderer smap tmap player_entity
    --display drink satiety
    display_thirst renderer smap tmap player_entity
    --display set
    display_set renderer smap tmap player_entity
    putStrLn "begining display inventory"
    display_inventory renderer smap tmap player_entity
    putStrLn "End display inventory"
    present renderer 
    endTime <- time
    putStrLn $ show $ fromIntegral (pvie player_entity) / fromIntegral (pvMax player_entity)
    let refreshTime = endTime - startTime 
    putStrLn log
    putStrLn $ show player_entity
    putStrLn "begining of behaviour enemies"
    let gameState2 = O.behaviour_envi gameState
    putStrLn "End of behaviour enemies"
    let deltaTime = endTime - startTime 
    putStrLn $ "x_player :"<>(show x_player) <> " y_player : "<>(show y_player)
    threadDelay $ 1000 -- 6000
    O.util_coord_test env gridd coord_player
    putStrLn "Begining of behaviour player"
    let (snd, gameState')= M.gameStep gameState2 kbd' deltaTime
    putStrLn "End of behaviour player"
    case (snd) of 
        ""  -> mempty :: IO ()
        "stepSound" -> PlaySound.playFile "./assets/sounds/footstep.ogg"
        "attack"    -> PlaySound.playFile "./assets/sounds/swing.wav" 
        "interact"  -> PlaySound.playFile "./assets/sounds/interact.wav"
        "keySound"  -> PlaySound.playFile "./assets/sounds/key.wav"
        "drinkSound"-> PlaySound.playFile "./assets/sounds/bottle.wav"
        "equipmentSound" -> PlaySound.playFile "./assets/sounds/interact.wav"
    --path <- System.Directory.getCurrentDirectory
    --putStrLn path 
    --PlaySound.withAudio $ do 
    --    snd
    unless ((finished_game gameState')|| K.keypressed KeycodeEscape kbd') (gameLoop frameRate renderer tmap smap kbd' gameState')
    